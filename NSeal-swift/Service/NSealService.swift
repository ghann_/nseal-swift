//
//  NSeal.swift
//  NSeal-swift
//
//  Created by ma.ghani on 3/8/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import Foundation
import SwiftTryCatch

class NSealService{
    
    typealias ReadQRCodeHandler = (Bool, NSealResult?, String?) -> ()
    typealias HTMLResulHandler = (Bool, String?, String?) -> ()
    typealias CustomResultHandler = (Bool, String, String) -> ()
    
    static let shared = NSealService()
    fileprivate let firebaseService = FirebaseService.shared
    fileprivate lazy var nSealVerifier = NSealVerifier()
    fileprivate lazy var nSealResult = NSealResult()
    
    func readQRCode(qrCodeString: String, handler: @escaping ReadQRCodeHandler) {
        SwiftTryCatch.try({
            self.nSealResult = self.nSealVerifier.verifyNSealQRCodeString(qrCodeString)
            self.firebaseService.logScanEvent(valid: self.nSealResult.isValid)
            handler(true, self.nSealResult, nil)
        }, catch: { err in
            print("Error: \(err!)")
            guard let nsealException = err as? NSealException else{
                print("Not NSealException")
                self.firebaseService.logQRScanErrorEvent(code: -1, description: err?.name.rawValue ?? "Internal Error")
                handler(false, nil , err?.description)
                return
            }
            print("NSealException \(nsealException)")
            self.firebaseService.logQRScanErrorEvent(code: nsealException.errorCode, description: nsealException.errorDescription)
            handler(false, nil, "\(nsealException.errorDescription)")
        }){}
    }
    
    func loadHTMLResult(of result: NSealResult, handler: HTMLResulHandler) {
        guard let htmlResultWithCertificate = nSealResult.htmlResultWithCertificate else {
            handler(false, nil, "An internal error occurred. Couldn't parsing HTMLString.".localized)
            return
        }
        handler(true, htmlResultWithCertificate, nil)
    }
    
    func checkCustomResult(with result: NSealResult, handler: CustomResultHandler) {
        handler(result.isValid ? true : false, result.customDescription, result.htmlCertificateOnly)
    }
}
