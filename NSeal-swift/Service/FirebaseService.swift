//
//  FirebaseService.swift
//  NSeal-swift
//
//  Created by ma.ghani on 3/5/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

class FirebaseService{
    static let shared = FirebaseService()
    
    func configure(){
        FirebaseApp.configure()
    }
    
    func logScanEvent(valid: Bool){
        Analytics.logEvent("QRCode_Scanned", parameters: [
            "Result"  : valid ? "Valid" : "Invalid"
        ])
    }
    
    func logQRScanErrorEvent(code: Int, description: String){
        Analytics.logEvent("QRCode_scanned_Error", parameters: [
            "ErrorCode" : code,
            "ErrorDescription" : description
        ])
    }
    
    func checkUpdateVersion(handler: @escaping (Double, Double)->Void){
        Database.database().reference().child("ApplicationInformation").observe(.value) { snapshot in
            guard let value = snapshot.value as? NSDictionary,
                  let latestVersion  = value["LatestVersion"] as? Double,
                  let minimumVersion = value["MinimumVersion"] as? Double else {return}
            handler(latestVersion, minimumVersion)
        }
    }
}
