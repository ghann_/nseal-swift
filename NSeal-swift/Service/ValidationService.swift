//
//  ValidationService.swift
//  NSeal-swift
//
//  Created by ma.ghani on 5/24/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import Foundation

struct Validation: Codable{
    
    var valid: String
    
    typealias validationHandler = (Result<Validation, URL>) -> ()
    enum Result<T, U>{
        case success(T, U)
        case failure(Error, U)
    }
    
    enum JSONError: String, Error {
        case NoData = "ERROR: no data"
        case ConversionFailed = "ERROR: conversion from JSON failed"
    }
    
    static func isValid(certificateType type: CertificateType, uniqueID: String, nsealID: String, handler: @escaping validationHandler) {
        guard let statusUrl = URL(string: "https://siki.lpjk.net/netrust/status.php?type=\(type.rawValue)&id=\(nsealID)"),
            let detailUrl = URL(string: "https://siki.lpjk.net/netrust/detail.php?type=\(type.rawValue)&id=\(uniqueID)") else {return}
        
        #if DEBUG
        print("statusUrl: \(statusUrl)")
        print("detailUrl: \(detailUrl)")
        #endif
        
        let session = URLSession(configuration: .default)
        let task = session.dataTask(with: URLRequest(url: statusUrl)) { data, response, error in
            if let error = error {
                handler(.failure(error, detailUrl))
            }
            
            do{
                guard let data = data else { throw JSONError.NoData }
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary else {
                    throw JSONError.ConversionFailed
                }
                var fixedValue = ""
                if let value = json["valid"] as? String {
                    fixedValue = value
                }else if let value = json["valid"] as? Int {
                    fixedValue = "\(value)"
                }
            
                handler(.success(Validation(valid: fixedValue), detailUrl))
            }catch{
                print(error)
                handler(.failure(error, detailUrl))
            }
        }
        task.resume()
    }
}
