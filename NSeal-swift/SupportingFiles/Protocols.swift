//
//  Protocols.swift
//  NSeal-swift
//
//  Created by ma.ghani on 3/2/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import Foundation
import AVKit

// SessionCapturable Protocol
protocol SessionCaptureable {
    var captureSession: AVCaptureSession? {get set}
    func configureCaptureSession()
    func configurePreviewLayer(with captureSession: AVCaptureSession)
    func startRunning()
    func stopRunning()
}

// TableViewEmptiable Protocol
protocol TableViewEmptiable{
    func configureTableView(tableView: UITableView)
    func configureEmptyTableView(tableView: UITableView, emptyText: String)
}

extension TableViewEmptiable{
    func configureTableView(tableView: UITableView){
        tableView.isScrollEnabled = true
        tableView.separatorStyle  = .singleLine
        tableView.backgroundView  = nil
        tableView.backgroundColor = .white
    }
    func configureEmptyTableView(tableView: UITableView, emptyText: String = "Looks like you haven’t done any scannings yet.".localized){
        let emptyStateImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
        emptyStateImageView.image = UIImage(named: "HistoryEmptyState")
        emptyStateImageView.contentMode = .center
        
        let noDataLabel: UILabel    = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width * 0.8, height: tableView.bounds.size.height))
        noDataLabel.center          = CGPoint(x: emptyStateImageView.frame.size.width / 2, y: emptyStateImageView.frame.size.height / 1.4)
        noDataLabel.text            = emptyText
        noDataLabel.numberOfLines   = 2
        noDataLabel.textColor       = Constants.Colors.unselectedTab
        noDataLabel.backgroundColor = .clear
        noDataLabel.textAlignment   = .center
        noDataLabel.font            = UIFont.systemFont(ofSize: 18, weight: .medium)
        
        emptyStateImageView.addSubview(noDataLabel)
        
        tableView.isScrollEnabled   = false
        tableView.backgroundView    = emptyStateImageView
        tableView.separatorStyle    = .none
    }
}

// SettingsEffect Delegate
protocol SettingsEffectDelegate: class{
    func didChangeValueSwitch(ofSwitch: SwitchToggle)
}

// SettingsLanguage Delegate
protocol SettingsLanguageDelegate: class {
    func didFinishChangeLanguage(to localization: Localization)
    func localizeTabBar()
}

// ScannerPhotoSelectable
protocol ScannerPhotoSelectable{
    func didFinishSelectPhotoFromLibrary(withImage image: UIImage)
    func decodeQRCode(sourceImage: UIImage, handler: (Bool, String?) -> Void)
}

// TableViewCellRemoveable
protocol TableViewCellRemoveable{
    func didSwipeCell(_ tableView: UITableView, at indexPath: IndexPath)
}

// ResultSegmentedDelegate
protocol SegmentedResultDelegate: class {
    func didChangeValue(displayResult: CertificateDisplayResult)
    func animateContainerView(displayResult: CertificateDisplayResult)
}
