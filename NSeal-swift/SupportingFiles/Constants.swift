//
//  Constants.swift
//  NSeal-swift
//
//  Created by ma.ghani on 3/7/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import Foundation

struct Constants{
    static let SegueOnboardMain = "SegueOnboardMain"
    static let SegueResultContainer = "SegueResultContainer"
    static let SegueResultContainerFromHistory = "SegueResultContainerFromHistory"
    static let SegueSettingsLanguage = "SegueSettingsLanguage"
    static let SegueAppInformation = "SegueAppInformation"
    static let databasePath = NSSearchPathForDirectoriesInDomains( .documentDirectory, .userDomainMask, true).first!
    
    struct Colors {
        static let defaultBlack = UIColor(red:0.23, green:0.23, blue:0.23, alpha:1.0)
        static let defaultGreen = UIColor(red:0.27, green:0.89, blue:0.48, alpha:1.0)
        static let defaultRed   = UIColor(red:0.89, green:0.27, blue:0.27, alpha:1.0)
        static let defaultBlue  = UIColor(red:0.29, green:0.56, blue:0.89, alpha:1.0)
        static let certValid    = UIColor(red:0.11, green:0.28, blue:0.62, alpha:1.0)
        static let certInvalid  = UIColor(red:0.74, green:0.20, blue:0.22, alpha:1.0)
        static let selectedTab  = UIColor(red:0.11, green:0.28, blue:0.51, alpha:1.0)
        static let unselectedTab = UIColor(red:0.59, green:0.59, blue:0.59, alpha:1.0)
        static let graySeparator = UIColor(red:0.92, green:0.92, blue:0.92, alpha:1.0)
        
        //ValidationView
        static let checking = UIColor(red:0.87, green:0.87, blue:0.87, alpha:1.0)
        static let notConnected = UIColor(red:0.29, green:0.29, blue:0.29, alpha:1.0)
        static let btnRevoked = UIColor(red:0.92, green:0.39, blue:0.41, alpha:1.0)
        static let btnValid = UIColor(red:0.29, green:0.56, blue:0.89, alpha:1.0)
        static let btnRetry = UIColor(red:0.61, green:0.61, blue:0.61, alpha:1.0)
    }
}
