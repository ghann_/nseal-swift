//
//  Extension.swift
//  NSeal-swift
//
//  Created by ma.ghani on 3/2/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import Foundation
import UIKit
import WebKit

// MARK: - UIViewController Extension
extension UIViewController{
    func showAlert(style: UIAlertControllerStyle = .alert, alertTitle title: String, alertMessage message: String, okText: String = "OK", cancelText: String = "Cancel".localized, okHandler: (()->Void)? = nil, cancelHandler: (()->Void)? = nil){
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        alert.addAction(UIAlertAction(title: okText, style: .destructive, handler: { (_) in okHandler?() }))
        alert.addAction(UIAlertAction(title: cancelText, style: .cancel, handler: { (_) in cancelHandler?() }))
        present(alert, animated: true, completion: nil)
    }
    func showAlert(alertTitle title: String, alertMessage message: String, cancelText: String = "Cancel".localized, cancelHandler: (()->Void)? = nil){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: cancelText, style: .cancel, handler: { (_) in cancelHandler?() }))
        present(alert, animated: true, completion: nil)
    }
    func showAlertForUpdate(with reason: UpdateReason, alertTitle title: String, alertMessage message: String, updateHandler: (()->Void)? = nil){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Update", style: .default, handler: { (_) in updateHandler?() }))
        
        switch reason {
        case .optional: alert.addAction(UIAlertAction(title: "Skip update".localized, style: .destructive, handler: nil))
        case .obsolete: ()
        }
        present(alert, animated: true, completion: nil)
    }
    func showLargeTitle(enable: Bool, withTitle title: String? = nil, toolBarHidden: Bool = true){
        if #available(iOS 11, *){
            self.navigationController?.navigationBar.prefersLargeTitles = enable ? true : false
        }
        self.title = title
        self.navigationController?.isToolbarHidden = toolBarHidden
    }
}

// MARK: - UIView Extension
extension UIView{
    func fadeInThenFadeOut(fadeDuration: Double = 0.25, handler: (()->Void)? = nil){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 15, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
            self.alpha = 1
        }) { _ in
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
                self.transform = CGAffineTransform.identity
                self.alpha = 0
            }, completion: { _ in
                handler?()
            })
        }
    }
    
    func getCertificateImage(handler: (UIImage) -> Void) {
        let webView = self as! WKWebView
        UIGraphicsBeginImageContextWithOptions(webView.scrollView.contentSize, false, 0);
        webView.scrollView.contentOffset = CGPoint.zero
        webView.scrollView.frame = CGRect(x: 0, y: 0, width:  webView.scrollView.contentSize.width + 200, height:  webView.scrollView.contentSize.height)
        webView.scrollView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        handler(image!)
    }
    
    func addShadow(){
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 4
    }
    
    func addConstraints(toView: UIView, leadingConst: CGFloat = 0, topConst: CGFloat = 0, trailingConst: CGFloat = 0, bottomConst: CGFloat = 0){
        self.translatesAutoresizingMaskIntoConstraints = false
        self.topAnchor.constraint(equalTo: toView.topAnchor, constant: topConst).isActive = true
        self.leadingAnchor.constraint(equalTo: toView.leadingAnchor, constant: leadingConst).isActive = true
        self.trailingAnchor.constraint(equalTo: toView.trailingAnchor, constant: -trailingConst).isActive = true
        self.bottomAnchor.constraint(equalTo: toView.bottomAnchor, constant: -bottomConst).isActive = true
    }
    
    func addConstraints(vc: UIViewController, superView: UIView,
                        leadingConst: CGFloat? = nil, topConst: CGFloat? = nil,
                        trailingConst: CGFloat? = nil, bottomConst: CGFloat? = nil,
                        widthConst: CGFloat, heightConst: CGFloat){
        self.translatesAutoresizingMaskIntoConstraints = false
        self.widthAnchor.constraint(equalToConstant: widthConst).isActive = true
        self.heightAnchor.constraint(equalToConstant: heightConst).isActive = true
        if #available(iOS 11, *){
            if let top = topConst {
                self.topAnchor.constraint(equalTo: superView.safeAreaLayoutGuide.topAnchor, constant: top).isActive = true
            }else{
                self.bottomAnchor.constraint(equalTo: superView.safeAreaLayoutGuide.bottomAnchor, constant: -bottomConst!).isActive = true
            }
            
            if let leading = leadingConst {
                self.leadingAnchor.constraint(equalTo: superView.safeAreaLayoutGuide.leadingAnchor, constant: leading).isActive = true
            }else{
                self.trailingAnchor.constraint(equalTo: superView.safeAreaLayoutGuide.trailingAnchor, constant: -trailingConst!).isActive = true
            }
        }else{
            if let top = topConst {
                self.topAnchor.constraint(equalTo: vc.topLayoutGuide.bottomAnchor, constant: top).isActive = true
            }else{
                self.bottomAnchor.constraint(equalTo: vc.bottomLayoutGuide.topAnchor, constant: -bottomConst!).isActive = true
            }
            
            if let leading = leadingConst {
                self.leadingAnchor.constraint(equalTo: superView.leadingAnchor, constant: leading).isActive = true
            }else{
                self.trailingAnchor.constraint(equalTo: superView.trailingAnchor, constant: -trailingConst!).isActive = true
            }
        }
    }
    
    func addConstraintsToSuperview(vc: UIViewController, superView: UIView, leadingConst: CGFloat = 0, topConst: CGFloat = 0, trailingConst: CGFloat = 0, bottomConst: CGFloat = 0){
        self.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 11, *){
            self.leadingAnchor.constraint(equalTo: superView.safeAreaLayoutGuide.leadingAnchor, constant: leadingConst).isActive = true
            self.topAnchor.constraint(equalTo: superView.safeAreaLayoutGuide.topAnchor, constant: topConst).isActive = true
            self.trailingAnchor.constraint(equalTo: superView.safeAreaLayoutGuide.trailingAnchor, constant: trailingConst).isActive = true
            self.bottomAnchor.constraint(equalTo: superView.safeAreaLayoutGuide.bottomAnchor, constant: bottomConst).isActive = true
        }else{
            self.leadingAnchor.constraint(equalTo: superView.leadingAnchor, constant: leadingConst).isActive = true
            self.topAnchor.constraint(equalTo: vc.topLayoutGuide.topAnchor, constant: topConst).isActive = true
            self.trailingAnchor.constraint(equalTo: superView.trailingAnchor, constant: trailingConst).isActive = true
            self.bottomAnchor.constraint(equalTo: vc.bottomLayoutGuide.bottomAnchor, constant: bottomConst).isActive = true
        }
    }
    
    func createTransition(transition: (() -> Void)?) {
        UIView.transition(with: self, duration: 0.25, options: .transitionCrossDissolve, animations: transition, completion: nil)
    }
    func fadeOut(handler: ((Bool)->())? = nil) {
        UIView.animate(withDuration: 0.25, delay: 0.25, options: .curveEaseInOut, animations: {
            self.alpha = 0
        }, completion: handler)
    }
}

// MARK: - Date Extension
extension Date{
    func formattedDate() -> String{
        let formatter =  DateFormatter()
        formatter.dateFormat = "dd-MMMM-yyyy HH:mm:ss z"
        return formatter.string(from: self)
    }
    var today: String{
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMMM yyyy - HH:mm"
        return formatter.string(from: self)
    }
}

// MARK: - UserDefaults Extension
extension UserDefaults{
    var hasLaunchBefore : Bool {
        return self.bool(forKey: #function)
    }
    
    func changeLanguage(to: Localization){
        UserDefaults.standard.set(to.i18, forKey: "i18n_language")
        UserDefaults.standard.synchronize()
    }
}

// MARK: - String Extension
extension String{
    var localized: String {
        if let _ = UserDefaults.standard.string(forKey: "i18n_language") {} else {
            UserDefaults.standard.set("id", forKey: "i18n_language")
            UserDefaults.standard.synchronize()
        }
        
        let lang = UserDefaults.standard.string(forKey: "i18n_language")
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
    
    var withoutTime: String {
        return String(self.dropLast(8))
    }
    
    var time: String{
        return self.components(separatedBy: " - ")[1]
    }
}

// MARK: - Bundle Extension
extension Bundle{
    var currentVersionNumber: String? {
        return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var currentBuildNumber: String?{
        return Bundle.main.infoDictionary?["CFBundleVersion"] as? String
    }
}

// MARK: - NSealResult Extension
extension NSealResult{
    var customDescription: String{
        let noTemplateString = "\n" + "Template is missing".localized
        if self.isValid {
            var validString = "You can trust this Certificate.".localized
//            if self.validToDate != nil {
//                validString += "\n" + "This is valid till ".localized + "\(self.validToDate.formattedDate())"
//            }
            validString +=  self.isTemplateMissing ? noTemplateString : ""
            
            return validString
        }else{
            var invalidString = "Do not trust this Certificate.".localized
            invalidString += self.isTampered ? "\nThis certificate has be tampered with." : ""
            invalidString += self.isRevoked ? "\n" + "This certificate has be revoked on ".localized + "\(self.revocationResult.date.formattedDate()) " + "due to".localized + " \(String(describing: self.revocationResult.reason))." : ""
            invalidString += self.isExpired ? "\n" + "This certificate has expired on ".localized + "\(self.validToDate.formattedDate())" : ""
            invalidString += self.isTemplateMissing ? noTemplateString : ""
            
            return invalidString
        }
    }
}

// MARK: - DefaultCertificateCell Extension
extension DefaultCertificateCell{
    func defaultCell(withText: String) -> DefaultCertificateCell {
        self.lblDescription.backgroundColor = .clear
        self.selectionStyle = .none
        self.lblDescription.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        self.lblDescription.textColor = UIColor(red:0.37, green:0.66, blue:0.05, alpha:1.0)
        self.lblDescription.text = withText
        return self
    }
    
    func defaultInfoCell(withText: String) -> DefaultCertificateCell{
        self.lblDescription.numberOfLines = 10
        self.lblDescription.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        self.lblDescription.textColor = Constants.Colors.defaultBlack
        self.lblDescription.text = withText
        return self
    }
}
