//
//  AppDelegate.swift
//  NSeal-swift
//
//  Created by ma.ghani on 3/7/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let firebaseService = FirebaseService.shared

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        Fabric.with([Crashlytics.self])
        firebaseService.configure()
        configureNavigation()
        configureTabBar()
        
        if UserDefaults.standard.hasLaunchBefore == false {
            UserDefaults.standard.set(true, forKey: "hasLaunchBefore")
            History.createDatabaseTable()
            UserSettings.createDatabaseTable()
            
            let onboardingVC = UIStoryboard(name: "Onboarding", bundle: nil).instantiateInitialViewController()
            self.window?.rootViewController = onboardingVC
            self.window?.makeKeyAndVisible()
        }
        
        return true
    }
    
    func configureNavigation(){
        let largeTextAttributes = [NSAttributedStringKey.foregroundColor : Constants.Colors.defaultBlack,
                                   NSAttributedStringKey.font: UIFont.systemFont(ofSize: 35, weight: .bold)]
        let textAttributes      = [NSAttributedStringKey.foregroundColor : Constants.Colors.defaultBlack,
                                   NSAttributedStringKey.font: UIFont.systemFont(ofSize: 20, weight: .bold)]
        
        if #available(iOS 11.0, *) {
            UINavigationBar.appearance().largeTitleTextAttributes = largeTextAttributes
        }
        UINavigationBar.appearance().titleTextAttributes = textAttributes
        UINavigationBar.appearance().tintColor = Constants.Colors.defaultBlack
    }
    func configureTabBar(){
        UITabBar.appearance().tintColor = Constants.Colors.selectedTab
        UITabBar.appearance().unselectedItemTintColor = Constants.Colors.unselectedTab
    }
    
    func checkForUpdate(){
        firebaseService.checkUpdateVersion { latestVersion, minimumVersion in
            guard let currentVersionNumber = Bundle.main.currentVersionNumber,
                  let currentVersion = Double("\(currentVersionNumber)"),
                  let rootViewController = self.window?.rootViewController else {
                    print("Error while getting application version")
                    return
            }
           
            if currentVersion < minimumVersion {
                rootViewController.showAlertForUpdate(with: UpdateReason.obsolete,
                                                      alertTitle: "Update Required".localized,
                                                      alertMessage: "Your NSeal-swift version is out of date, minimum application version is ".localized + "\(minimumVersion)"){
                    print("Updating")
                }
            }else if currentVersion < latestVersion{
                rootViewController.showAlertForUpdate(with: UpdateReason.optional,
                                                      alertTitle: "Update Available".localized,
                                                      alertMessage: "NSeal-swift \(latestVersion) " + "is available".localized){
                    print("Updating")
                }
            }
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        checkForUpdate()
    }
}
