//
//  AudioPlayer.swift
//  NSeal-swift
//
//  Created by ma.ghani on 3/3/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import AVFoundation
import AudioToolbox

class AudioPlayer{
    static let shared = AudioPlayer()
    fileprivate var player: AVAudioPlayer?
    fileprivate var userSettings: UserSettings? {
        guard let settings =  UserSettings.getUserSettings() else{return nil}
        return settings
    }
    
    func playDetectedSound(handler: (()->Void)? = nil){
        guard let url = Bundle.main.url(forResource: "detected", withExtension: "m4a") else {
            print("Error: Audio file not found.")
            return
        }
        playSound(content: url)
        handler?()
    }
    
    private func playSound(content url: URL){
        do{
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.m4a.rawValue)

            guard let player = player else { return }
            _ = userSettings!.soundEffect ? player.play() : nil
            _ = userSettings!.vibration ? AudioServicesPlayAlertSound(kSystemSoundID_Vibrate) : nil
        
        }catch let error{
            print("error: \(error.localizedDescription)")
        }
    }
}

