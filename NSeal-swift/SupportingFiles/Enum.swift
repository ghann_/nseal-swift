//
//  Enum.swift
//  NSeal-swift
//
//  Created by ma.ghani on 3/2/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import Foundation

enum CertificateType: String {
    case SKA1 = "ska1", SBU1 = "sbu1", SKT1 = "skt1"
    
    static func getType(rawString: String) -> CertificateType?{
        switch rawString {
        case "SKA1": return .SKA1
        case "SBU1": return .SBU1
        case "SKT1": return .SKT1
        default: return nil
        }
    }
    
    var title: String {
        switch self {
        case .SKA1: return "SERTIFIKAT KEAHLIAN"
        case .SBU1: return "SERTIFIKAT BADAN USAHA JASA PELAKSANA KONSTRUKSI"
        case .SKT1: return "SERTIFIKAT KETERAMPILAN KERJA"
        }
    }
    
    var titleSimplified: String{
        switch self {
        case .SKA1: return "Sertifikat Keahlian"
        case .SBU1: return "Sertifikat Badan Usaha"
        case .SKT1: return "Sertifikat Keterampilan Kerja"
        }
    }
    
    var numberOfRowHeaderTitles: Int{
        return self.listOfRowHeaderTitles.count
    }
    
    var numberOfRowHeaderTitlesWithTopHeader: Int{
        return self.listOfRowHeaderTitles.count + 1
    }
    
    var listOfRowHeaderTitles: [String] {
        switch self {
        case .SKA1:
            return ["Nama", "", "Subbidang - Kualifikasi", "Nomor Registrasi", "", "Tempat Ditetapkan", "Tanggal Ditetapkan",
                    "Jabatan Penandatangan", "Nama Penandatangan", "Asosiasi", "Nomor Berita Acara Asosiasi", ""]
        case .SBU1:
            return ["Nama Badan Usaha", "Nama Penanggung Jawab / PJBU", "Alamat", "Kabupaten", "Provinsi", "Nomor Telepon",
                    "Email", "NPWP", "Jenis Badan Usaha", "Kategori Badan Usaha", "Kekayaan Bersih", "Kode Pos", "Nomor Fax",
                    "Nomor Registrasi", "", "Tempat Ditetapkan", "Tanggal Ditetapkan", "Jabatan Penandatangan", "Nama Penandatangan",
                    "Klasifikasi SBU", "Golongan SBU", "Asosiasi", "Rincian"]
        case .SKT1:
            return ["Nama", "Alamat", "Subbidang", "Kualifikasi", "Tempat Ditetapkan", "Tanggal Ditetapkan",
                    "Jabatan Penandatangan", "Nama Penandatangan", "Nomor Registrasi", "Asosiasi", "Kompetensi Kerja"]
        }
    }
    
    var infoMessage: String {
        switch self {
        case .SKA1:
            return "Sesuai dengan Undang-Undang No. 18 Tahun 1999 tentang Jasa Konstruksi dan Peraturan Pemerintah Nomor 28 Tahun 2000 sebagaimana diubah dengan Peraturan Pemerintah Nomor 4 Tahun 2010 dan Peraturan Pemerintah Nomor 92 Tahun 2010, dengan ini Lembaga Pengembangan Jasa Konstruksi menetapkan bahwa :"
        case .SBU1:
            return "Berdasarkan ketentuan pasal 8 huruf b dan berdasarkan pasal 17 ayat (4) dan ayat (5) Undang - Undang No. 18 Tahun 1999 tentang Jasa Konstruksi dengan ini Lembaga Pengembangan Jasa Konstruksi menetapkan bahwa :"
        case .SKT1:
            return ""
        }
    }
    
    var extraInfoMessage: String {
        switch self  {
        case .SBU1:
            return "Dinyatakan memiliki kemampuan dengan klasifikasi dan kualifikasi sebagaimana yang tercantum di halaman belakang sertifikat ini.\nSertifikat ini diterbitkan pertama tanggal 21 April 2017 berlaku sampai dengan tanggal 20 April 2020 dengan kewajiban registrasi ulang tahun ke-2 paling lambat tanggal 20 Oktober 2018 dan registrasi ulang tahun ke-3 paling lambat tanggal 20 Oktober 2019"
        default:
            return ""
        }
    }
    
}

enum QRCodeError: String{
    case NSealErrorCodeDataBaseInvalid = "This certificate is either invalid or the QR Code is old version. Please convert to the new electronic certificate. More info at bit.ly/registrasilpjk (0)" // "Database signature miss-matched, Database is missing or has been tampered with."
    case NSealErrorCodePayloadInvalid = "This certificate is either invalid or the QR Code is old version. Please convert to the new electronic certificate. More info at bit.ly/registrasilpjk (1)" // "nSeal payload not found, invalid nSeal scanned."
    case NSealErrorCodePayloadSignatureMissing = "This certificate is either invalid or the QR Code is old version. Please convert to the new electronic certificate. More info at bit.ly/registrasilpjk (2)" // "nSeal signature not found, corrupted nSeal scanned."
    case NSealErrorCodePayloadDataMissing = "This certificate is either invalid or the QR Code is old version. Please convert to the new electronic certificate. More info at bit.ly/registrasilpjk (3)" // "Seal data not found, corrupted nSeal scanned."
    
    var errorCode: Int{
        switch self {
        case .NSealErrorCodeDataBaseInvalid: return 0
        case .NSealErrorCodePayloadInvalid: return 1
        case .NSealErrorCodePayloadSignatureMissing: return 2
        case .NSealErrorCodePayloadDataMissing: return 3
        }
    }
    
    var localizedDescription: String {
        switch Localization.getLanguage() {
        case .English: return self.rawValue
        case .Indonesia:
            switch self {
            case .NSealErrorCodeDataBaseInvalid:
                //return "Database signature tidak cocok, Database tidak ditemukan atau rusak."
                return "Sertifikat tidak valid atau masih memakai QR Code lama. Mohon melakukan konversi ke sertifikat berbentuk dokumen elektronik, keterangan lebih lengkap ada di bit.ly/registrasilpjk (0)"
            case .NSealErrorCodePayloadInvalid:
                //return "nSeal payload tidak ditemukan, QRCode tidak valid."
                return "Sertifikat tidak valid atau masih memakai QR Code lama. Mohon melakukan konversi ke sertifikat berbentuk dokumen elektronik, keterangan lebih lengkap ada di bit.ly/registrasilpjk (1)"
            case .NSealErrorCodePayloadSignatureMissing:
                //return "nSeal signature tidak ditemukan, QRCode rusak."
                return "Sertifikat tidak valid atau masih memakai QR Code lama. Mohon melakukan konversi ke sertifikat berbentuk dokumen elektronik, keterangan lebih lengkap ada di bit.ly/registrasilpjk (2)"
            case .NSealErrorCodePayloadDataMissing:
                //return "Data seal tidak ditemukan, QRCode rusak."
                return "Sertifikat tidak valid atau masih memakai QR Code lama. Mohon melakukan konversi ke sertifikat berbentuk dokumen elektronik, keterangan lebih lengkap ada di bit.ly/registrasilpjk (3)"
            }
        }
    }
}

enum CertificateDisplayResult: String{
    case `default`
    case custom
}

enum AppInformation: String{
    case about
    case FAQ
    case TNC
    
    var url: URL? {
        switch self {
        case .FAQ: return URL(string: "http://lpjk.net/apps/faq.html")
        case .TNC: return URL(string: "http://lpjk.net/apps/privacy.html")
        default: return nil
        }
    }
}

enum ResultOrigin{
    case create, view(Int64)
}

enum UpdateReason: String{
    case obsolete, optional
}

enum Localization: String{
    case Indonesia, English
    
    var i18: String {
        return self == .English ? "en" : "id"
    }
    
    static func getLanguage() -> Localization {
        let language = UserDefaults.standard.string(forKey: "i18n_language")!
        return language == "en" ? .English : .Indonesia
    }
}

enum SwitchToggle{
    case sound(Bool)
    case vibration(Bool)
}
