//
//  OnboardingViewController.swift
//  NSeal-swift
//
//  Created by ma.ghani on 3/12/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import UIKit

class OnboardingViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnStart: UIButton!
    @IBOutlet weak var btnIDLocalization: UIButton!
    @IBOutlet weak var btnENLocalization: UIButton!
    @IBAction func btnChangeLocalizationTapped(_ sender: UIButton!){
        switch sender.tag {
        case 0:
            didFinishChangeLanguage(to: .Indonesia)
            sender.setImage(UIImage(named: "idSelected"), for: .normal)
            btnENLocalization.setImage(UIImage(named: "enDeselected"), for: .normal)
        case 1:
            didFinishChangeLanguage(to: .English)
            sender.setImage(UIImage(named: "enSelected"), for: .normal)
            btnIDLocalization.setImage(UIImage(named: "idDeselected"), for: .normal)
        default: ()
        }
        localizeOnBoarding()
    }
    @IBAction func btnStartTapped(_ sender: UIButton!){
        performSegue(withIdentifier: Constants.SegueOnboardMain, sender: nil)
    }
    
    func localizeOnBoarding(){
        btnStart.setTitle("START".localized, for: .normal)
        lblTitle.createTransition {
            self.lblTitle.text = "Scan QR Code to validate certificate".localized
        }
        lblDescription.createTransition {
            self.lblDescription.text = "Your scans are saved in the Scan History".localized
        }
    }
    
    func configureLocalizationButton(){
        switch Localization.getLanguage() {
        case .English:
            btnENLocalization.setImage(UIImage(named: "enSelected"), for: .normal)
            btnIDLocalization.setImage(UIImage(named: "idDeselected"), for: .normal)
        case .Indonesia:
            btnENLocalization.setImage(UIImage(named: "enDeselected"), for: .normal)
            btnIDLocalization.setImage(UIImage(named: "idSelected"), for: .normal)
        }
    }
}

// MARK: - Life Cycle
extension OnboardingViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeOnBoarding()
        configureLocalizationButton()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

// MARK: - SettingsLanguageDelegate
extension OnboardingViewController: SettingsLanguageDelegate {
    func didFinishChangeLanguage(to localization: Localization) {
        UserSettings.saveLocalization(localizationString: localization.rawValue)
        UserDefaults().changeLanguage(to: localization)
        localizeTabBar()
    }
    func localizeTabBar(){ }
}
