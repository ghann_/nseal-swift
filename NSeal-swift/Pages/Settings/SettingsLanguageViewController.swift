//
//  SettingsLanguageViewController.swift
//  NSeal-swift
//
//  Created by ma.ghani on 3/9/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import UIKit

class SettingsLanguageViewController: UIViewController {
    var currentLocalization: Localization = .English
    weak var delegate: SettingsLanguageDelegate?
    
    @IBOutlet weak var languageSettingsTableView: UITableView!
    
    func configureView() {
        showLargeTitle(enable: false, withTitle: "Language".localized)
        languageSettingsTableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
    }
}

// MARK: - Life Cycle
extension SettingsLanguageViewController{
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        configureView()
    }
}

// MARK: - TableView DataSource
extension SettingsLanguageViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LanguageCell", for: indexPath) as! DefaultSettingsCell
        cell.lblOption.text = indexPath.row == 0 ? "English".localized : "Indonesia"
        
        if currentLocalization == .English && indexPath.row == 0 {
            cell.accessoryType = .checkmark
        }else if currentLocalization == .Indonesia && indexPath.row == 1 {
            cell.accessoryType = .checkmark
        }
        
        return cell
    }
}

// MARK: - TableView Delegate
extension SettingsLanguageViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = .white
        
        let headerLabel = UILabel(frame: CGRect(x: 20, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
        headerLabel.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        headerLabel.textColor = Constants.Colors.defaultBlack
        headerLabel.text = "Application Language".localized
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        _ = tableView.visibleCells.map{ $0.accessoryType = .none }
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        
        let localization: Localization = indexPath.row == 0 ? .English : .Indonesia
        delegate?.didFinishChangeLanguage(to: localization)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Application Language".localized
    }
}
