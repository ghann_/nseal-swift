//
//  SettingsCell.swift
//  NSeal-swift
//
//  Created by ma.ghani on 3/7/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import UIKit

class SettingsEffectsCell: UITableViewCell {

    weak var settingsEffectDelegate: SettingsEffectDelegate?
    
    @IBOutlet weak var lblOption: UILabel!
    @IBOutlet weak var swtchSoundEffect: UISwitch!
    @IBOutlet weak var swtchVibrationEffect: UISwitch!
    @IBAction func toggleSoundEffect(_ sender: UISwitch){
        settingsEffectDelegate?.didChangeValueSwitch(ofSwitch: .sound(swtchSoundEffect.isOn))
    }
    @IBAction func toggleVibrationEffect(_ sender: UISwitch){
        settingsEffectDelegate?.didChangeValueSwitch(ofSwitch: .vibration(swtchVibrationEffect.isOn))
    }
}
