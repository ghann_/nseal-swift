//
//  SettingsViewController.swift
//  NSeal-swift
//
//  Created by ma.ghani on 3/7/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    fileprivate let audioPlayer = AudioPlayer.shared
    fileprivate var userSettings: UserSettings? {
        guard let settings =  UserSettings.getUserSettings() else{
            showAlert(alertTitle: "Error", alertMessage: "An internal error occurred. Couldn't get user settings.".localized)
            return nil
        }
        return settings
    }
    
    fileprivate lazy var footerView: UIView? = {
        var tableViewHeight: CGFloat {
            settingsTableView.layoutIfNeeded()
            return settingsTableView.contentSize.height
        }
        
        let settingsTableSize = settingsTableView.frame.size
        let rectLastRow = settingsTableView.rectForRow(at:  IndexPath(row: 0, section: 2))
        let lastRowFrame = settingsTableView.convert(rectLastRow, to: view)
        let tabBarYPos = self.tabBarController!.tabBar.frame.origin.y
        let emptySpaceHeight = tabBarYPos - (tableViewHeight + lastRowFrame.size.height + 100)
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: settingsTableSize.width, height: emptySpaceHeight))
        let lblVersion = UILabel(frame: CGRect(x: 0, y: footerView.frame.maxY * 0.7, width: UIScreen.main.bounds.width, height: 14))
        lblVersion.textAlignment = .center
        lblVersion.font = UIFont.systemFont(ofSize: 14, weight: .light)
        lblVersion.textColor = Constants.Colors.unselectedTab
        lblVersion.text = "VERSION".localized
        
        let lblVNum = UILabel(frame: CGRect(x: 0, y: lblVersion.frame.maxY, width: UIScreen.main.bounds.width, height: 20))
        lblVNum.textAlignment = .center
        lblVNum.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        lblVNum.textColor = Constants.Colors.unselectedTab
        lblVNum.text = "\(Bundle.main.currentVersionNumber ?? "")"
    
        footerView.addSubview(lblVersion)
        footerView.addSubview(lblVNum)
        
        return footerView
    }()
    
    @IBOutlet weak var settingsTableView: UITableView!
    
}

// MARK: - Life Cycle
extension SettingsViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.view.backgroundColor = .white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        showLargeTitle(enable: true, withTitle: "Settings".localized)
        settingsTableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        settingsTableView.tableFooterView = footerView
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.SegueSettingsLanguage {
            let settingsLanguageVC = segue.destination as! SettingsLanguageViewController
            settingsLanguageVC.delegate = self
            settingsLanguageVC.currentLocalization = userSettings!.localization
        }
    }
}

// MARK: - TableView DataSource
extension SettingsViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return 1
        case 1: return 2
        case 2: return 1
        default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        switch indexPath.section {
        case 0:
            switch indexPath.row{
            case 0:
                let defaultCell = tableView.dequeueReusableCell(withIdentifier: "DefaultSettingsCell", for: indexPath) as! DefaultSettingsCell
                defaultCell.lblOption.text = "Language".localized
                defaultCell.lblExtra.text = userSettings?.localization.rawValue
                defaultCell.accessoryType = .disclosureIndicator
                cell = defaultCell
            default: ()
            }
        case 1:
            switch indexPath.row{
            case 0:
                let soundCell = tableView.dequeueReusableCell(withIdentifier: "SoundEffectsCell", for: indexPath) as! SettingsEffectsCell
                soundCell.settingsEffectDelegate = self
                soundCell.lblOption.text = "Sound Effects".localized
                soundCell.swtchSoundEffect.isOn = userSettings!.soundEffect
                cell = soundCell
            case 1:
                let vibrationCel = tableView.dequeueReusableCell(withIdentifier: "VibrationsCell", for: indexPath) as! SettingsEffectsCell
                vibrationCel.settingsEffectDelegate = self
                vibrationCel.lblOption.text = "Vibration".localized
                vibrationCel.swtchVibrationEffect.isOn = userSettings!.vibration
                cell = vibrationCel
            default: ()
            }
        case 2:
            let defaultCell = tableView.dequeueReusableCell(withIdentifier: "DefaultSettingsCell", for: indexPath) as! DefaultSettingsCell
            defaultCell.lblOption.text = "Clear all scan history".localized
            defaultCell.lblOption.textColor = Constants.Colors.defaultBlue
            cell = defaultCell
        default: return cell
        }
        return cell
    }
}

// MARK: - TableView Delegate
extension SettingsViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        let headerLabel = UILabel(frame: CGRect(x: 20, y: 0, width: settingsTableView.bounds.size.width, height: settingsTableView.bounds.size.height))
        headerLabel.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        headerLabel.textColor = Constants.Colors.defaultBlack
        headerLabel.text = self.tableView(settingsTableView, titleForHeaderInSection: section)
        headerLabel.sizeToFit()
        
        headerView.backgroundColor = .white
        headerView.addSubview(headerLabel)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0: return "Application Settings".localized
        case 1: return "Scan Settings".localized
        case 2: return "History Settings".localized
        default: return ""
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            indexPath.row == 0 ? performSegue(withIdentifier: Constants.SegueSettingsLanguage, sender: nil) : nil
        case 2:
            showAlert(style: .actionSheet, alertTitle: "Clear History".localized, alertMessage: "Clear all history?".localized, okText: "Proceed".localized, okHandler: {
                History.dropTable {
                    self.showAlert(alertTitle: "Success".localized, alertMessage: "QRCode history has been deleted.".localized, cancelText: "OK")
                }
            }, cancelHandler: nil)
        default: ()
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}

// MARK: - SettingsEffectDelegate
extension SettingsViewController: SettingsEffectDelegate {
    func didChangeValueSwitch(ofSwitch: SwitchToggle) {
        switch ofSwitch {
        case .sound(let state):
            UserSettings.saveSoundEffect(enable: state)
            audioPlayer.playDetectedSound()
        case .vibration(let state):
            UserSettings.saveVibration(enable: state)
            audioPlayer.playDetectedSound()
        }
    }
}

// MARK: - SettingsLanguageDelegate
extension SettingsViewController: SettingsLanguageDelegate {
    func didFinishChangeLanguage(to localization: Localization) {
        UserSettings.saveLocalization(localizationString: localization.rawValue)
        settingsTableView.reloadData()
        UserDefaults().changeLanguage(to: localization)
        localizeTabBar()
    }
    func localizeTabBar(){
        if let tabBarItems = tabBarController?.tabBar.items {
            tabBarItems.forEach{ $0.title = $0.title?.localized }
        }
    }
}
