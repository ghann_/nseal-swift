//
//  DefaultSettingsCell.swift
//  NSeal-swift
//
//  Created by ma.ghani on 3/9/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import UIKit

class DefaultSettingsCell: UITableViewCell {

    @IBOutlet weak var lblOption: UILabel!
    @IBOutlet weak var lblExtra: UILabel!
    
}
