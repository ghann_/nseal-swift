//
//  HistoryViewController.swift
//  NSeal-swift
//
//  Created by ma.ghani on 3/7/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController {

    fileprivate var selectedQRCode: String?
    fileprivate var selectedCertificateId: Int64 = 0
    fileprivate var historyList: [HistoryList] = []
    fileprivate var editRowFlag: Bool = false
    fileprivate var selectHistoryButton: UIBarButtonItem?
    
    fileprivate lazy var trashBarButton: [UIBarButtonItem] = {
        var items = [UIBarButtonItem]()
        let trash = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(removeSelectedHistory))
        trash.tintColor = .red
        items.append(UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil))
        items.append(trash)
        
        return items
    }()
    
    @IBOutlet weak var actvInficatorView: UIView!
    @IBOutlet weak var historyTable: UITableView!
    
    func configureToolBarButton(isHidden: Bool){
        navigationController?.isToolbarHidden = isHidden
        tabBarController?.tabBar.isHidden = !isHidden
    }
    
    func configureBarButton(){
        selectHistoryButton = UIBarButtonItem(title: "Select".localized, style: .plain, target: self, action: #selector(btnEditTapped))
        navigationItem.rightBarButtonItem = selectHistoryButton
    }

    func configureView(){
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(Int.min)) {
            self.tabBarController?.tabBar.isHidden = false
        }
        actvInficatorView.alpha = 1
        navigationController?.toolbar.clipsToBounds = false
        toolbarItems = trashBarButton
        configureBarButton()
        showLargeTitle(enable: true, withTitle: "History".localized, toolBarHidden: true)
        historyTable.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
    }
     
    func fetchHistories(handler: (()->Void)? = nil){
        DispatchQueue.main.async {
            self.historyList.removeAll()
            self.historyList = HistoryList.getHistoryList()
            self.selectHistoryButton!.isEnabled = self.historyList.count > 0 ? true : false
            self.historyTable.reloadData()
            handler?()
        }
    }
    
    @objc func btnEditTapped(){
        editRowFlag = !editRowFlag
        selectHistoryButton!.title = editRowFlag ? "Done".localized : "Select".localized
        historyTable.setEditing(editRowFlag, animated: true)
        editRowFlag ? configureToolBarButton(isHidden: false) : configureToolBarButton(isHidden: true)
    }
    
    @objc func removeSelectedHistory(){
        guard let selectedIndexPath = historyTable.indexPathsForSelectedRows, !selectedIndexPath.isEmpty else {
            showAlert(alertTitle: "Error", alertMessage: "Please select at least one item".localized, cancelText: "Dismiss".localized)
            return
        }
        
        let suffix = "\(selectedIndexPath.count > 1 ? "s" : "")"
        showAlert(style: .actionSheet, alertTitle: "Remove Certificate\(suffix)".localized,
                  alertMessage: "Remove selected certificate\(suffix) from history?".localized, okText: "Remove".localized, okHandler: {
            for indexPath in selectedIndexPath{
                History.removeHistory(forId: self.historyList[indexPath.section].histories[indexPath.row].id)
            }
            self.fetchHistories{
                if self.historyList.isEmpty{
                    self.editRowFlag = false
                    self.selectHistoryButton!.title = "Select".localized
                    self.historyTable.setEditing(false, animated: true)
                    self.configureToolBarButton(isHidden: true)
                }
            }
        }, cancelHandler: nil)
    }
}

// MARK: - Life Cycle
extension HistoryViewController{
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        configureView()
        fetchHistories(){
            self.actvInficatorView.fadeOut()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.SegueResultContainerFromHistory {
            guard let resultContainerVC = segue.destination as? ResultContainerViewController,
                let selectedQRCode = selectedQRCode else { return }
            resultContainerVC.qrCodeString = selectedQRCode
            resultContainerVC.resultOrigin = .view(selectedCertificateId)
        }
    }
}

// MARK: - TableView DataSource
extension HistoryViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        historyList.count > 0 ? configureTableView(tableView: tableView) : configureEmptyTableView(tableView: tableView)
        return historyList.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyList[section].histories.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let historyCell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell", for: indexPath) as! HistoryCell
        let currentHistory = historyList[indexPath.section].histories[indexPath.row]

        //historyCell.lblTime.text = currentHistory.date.time
        historyCell.lblTitle.text = currentHistory.title
        historyCell.lblDescription.text = currentHistory.description
        historyCell.imgValidity.image = currentHistory.isValid ? UIImage(named: "valid") : Localization.getLanguage() == .English ? UIImage(named: "invalid") : UIImage(named: "tidakValid")

        return historyCell
    }
}

// MARK: - TableView Delegate
extension HistoryViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        let headerLabel = UILabel(frame: CGRect(x: 20, y: 10, width: historyTable.bounds.size.width, height: historyTable.bounds.size.height))
        headerLabel.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        headerLabel.textColor = Constants.Colors.defaultBlack
        headerLabel.text = self.tableView(tableView, titleForHeaderInSection: section)
        headerLabel.sizeToFit()

        headerView.backgroundColor = .white
        headerView.addSubview(headerLabel)

        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return historyList[section].date
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !tableView.isEditing{
            tableView.deselectRow(at: indexPath, animated: true)
            selectedQRCode = historyList[indexPath.section].histories[indexPath.row].qr_string
            selectedCertificateId = historyList[indexPath.section].histories[indexPath.row].id
            performSegue(withIdentifier: Constants.SegueResultContainerFromHistory, sender: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            didSwipeCell(tableView, at: indexPath)
        }
    }
}

// MARK: - TableViewCellRemoveable Protocol
extension HistoryViewController: TableViewCellRemoveable{
    func didSwipeCell(_ tableView: UITableView, at indexPath: IndexPath) {
        let rowCount = self.tableView(tableView, numberOfRowsInSection: indexPath.section)
        let sectionCount = tableView.numberOfSections
        let indexSet = IndexSet(arrayLiteral: indexPath.section)
        
        History.removeHistory(forId: historyList[indexPath.section].histories[indexPath.row].id)
        
        switch rowCount {
        case 1 where sectionCount == 1:
            historyList.removeAll()
            History.dropTable()
            tableView.reloadData()
        case 1:
            historyList.remove(at: indexPath.section)
            tableView.deleteSections(indexSet, with: .left)
            tableView.reloadData()
        case 2...:
            historyList[indexPath.section].histories.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .left)
            tableView.reloadSections(indexSet, with: .none)
        default: ()
        }
    }
}

// MARK: - TablViewEmptiable Protocol
extension HistoryViewController: TableViewEmptiable {}
