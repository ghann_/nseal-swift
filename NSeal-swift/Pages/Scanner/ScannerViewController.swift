//
//  ScannerViewController.swift
//  NSeal-swift
//
//  Created by ma.ghani on 3/2/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import UIKit
import AVKit

class ScannerViewController: UIViewController {
    fileprivate var previewLayer: AVCaptureVideoPreviewLayer?
    fileprivate var dimmingLayer: CALayer?
    fileprivate var maskingLayer: CAShapeLayer?
    fileprivate var QRCodeString: String?
    var captureSession: AVCaptureSession?
    fileprivate var captureMetadataOutput: AVCaptureMetadataOutput?
    fileprivate var loadingView: UIView!

    fileprivate var flashFlag: Bool = true
    fileprivate let audioPlayer = AudioPlayer.shared
    
    fileprivate lazy var indicatorContainerView: UIView = {
        let view = UIView(frame: self.view.bounds)
        view.backgroundColor = .black
        view.addSubview(activityIndicatorView)
        return view
    }()
    fileprivate lazy var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
        activityIndicator.center = view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        return activityIndicator
    }()
    
    @IBOutlet weak var btnFlash: UIButton!
    @IBOutlet weak var btnLibrary: UIButton!
    @IBOutlet weak var frameImageView: UIImageView!
    @IBOutlet weak var cameraView: UIView!
    
    @IBAction func btnInfoTapped(_ sender: UIBarButtonItem!) {
        let onBoardingVC = UIStoryboard(name: "Onboarding", bundle: nil).instantiateInitialViewController()
        present(onBoardingVC!, animated: true, completion: nil)
    }
    
    @IBAction func btnFlashTapped(_ sender: UIButton!) {
        let image = flashFlag ? UIImage(named: "Flash On") : UIImage(named: "Flash Off")
        sender.setImage(image, for: .normal)
        toggleFlashlight(with: flashFlag)
        flashFlag = !flashFlag
    }
    func toggleFlashlight(with option: Bool) {
        guard let device = AVCaptureDevice.default(for: .video) else {return}
        if device.hasTorch{
            do{
                try device.lockForConfiguration()
                device.torchMode = option ? .on : .off
                device.unlockForConfiguration()
            }catch{
                showAlert(alertTitle: "Error: Flashlight could not be used".localized, alertMessage: "\(error)")
                print("Flashlight could not be used")
            }
        }else{
            if option == true { // Show this error only if user is trying to switch flashlight on
                showAlert(alertTitle: "Error", alertMessage: "Flashlight is not available".localized)
                print("Flashlight is not available")
            }
        }
    }
    
    @IBAction func getImageFromLibrary(_ sender: UIButton!) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }

    func configureDimmingLayer(){
        configureMaskingLayer()
        
        dimmingLayer = CALayer()
        dimmingLayer?.frame = cameraView.bounds
        dimmingLayer?.opacity = 0.5
        dimmingLayer?.backgroundColor = Constants.Colors.defaultBlack.cgColor
        
        dimmingLayer?.mask = maskingLayer
        cameraView.layer.addSublayer(dimmingLayer!)
    }
    
    func configureMaskingLayer(){
        maskingLayer = CAShapeLayer()
        maskingLayer?.fillRule = kCAFillRuleEvenOdd
    }
    
    func updateMaskLayerRectArea(){
        let path = CGMutablePath()
        let constant = 1 * UIScreen.main.scale
        let frame = CGRect(x: self.frameImageView.frame.minX + constant,
                           y: self.frameImageView.frame.minY + constant,
                           width: self.frameImageView.frame.width - (2 * constant),
                           height: self.frameImageView.frame.height - (2 * constant))
        
        path.addRect(self.cameraView.convert(frame, to: self.view))
        path.addRect(self.cameraView.bounds)
        maskingLayer?.path = path
    }
    
    func rearrangeViews() {
        DispatchQueue.main.async {
            self.view.subviews.forEach{
                if $0 != self.cameraView { self.view.bringSubview(toFront: $0) }
            }
            self.viewDidLayoutSubviews()
        }
    }
    
    func localizeTabBar() {
        if let tabBarItems = tabBarController?.tabBar.items {
            tabBarItems.forEach{ $0.title = $0.title?.localized }
        }
    }
    
    func fadeInScannerView() {
        loadingView.backgroundColor = .clear
        view.sendSubview(toBack: self.loadingView)
    }
    
    func configureIndicatorContainerView() {
        loadingView = indicatorContainerView
        view.addSubview(loadingView)
        view.bringSubview(toFront: loadingView)
    }
    
    func configureScanner(handler: @escaping ()->()) {
        checkCameraAuthorization()
        configureCaptureSession()
        configureDimmingLayer()
        rearrangeViews()
        localizeTabBar()
        startRunning()
        
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                handler()
            }
        }
    }
}

// MARK: - Life Cycle
extension ScannerViewController{
    override func viewDidLoad() {
        btnFlash.addConstraints(vc: self, superView: view, leadingConst: 15, bottomConst: 15, widthConst: 40, heightConst: 40)
        btnLibrary.addConstraints(vc: self, superView: view, trailingConst: 15, bottomConst: 15, widthConst: 40, heightConst: 40)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        showLargeTitle(enable: true, withTitle: "Scan", toolBarHidden: true)
        configureIndicatorContainerView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        configureScanner {
            self.fadeInScannerView()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        stopRunning()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        DispatchQueue.main.async {
            self.dimmingLayer?.frame = self.cameraView.bounds
            self.previewLayer?.frame = self.cameraView.bounds
            self.captureMetadataOutput?.rectOfInterest = self.previewLayer!.metadataOutputRectConverted(fromLayerRect: self.frameImageView.frame)
            self.updateMaskLayerRectArea()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.SegueResultContainer{
            guard let resultContainerVC = segue.destination as? ResultContainerViewController,
                let QRCodeString    = QRCodeString else { return }
            resultContainerVC.qrCodeString = QRCodeString
            resultContainerVC.resultOrigin = .create
        }
    }
}

// MARK: - SessionCaptureable
extension ScannerViewController: SessionCaptureable {
    func configureCaptureSession() {
        guard let device = AVCaptureDevice.default(for: .video) else {return}
        if device.isAutoFocusRangeRestrictionSupported{
            do {
                try device.lockForConfiguration()
                device.autoFocusRangeRestriction = .near
                device.unlockForConfiguration()
            } catch{
                print(error)
            }
        }
        
        do {
            captureSession = AVCaptureSession()
            let input = try AVCaptureDeviceInput(device: device)
            captureSession?.addInput(input)
            captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput!)
            captureMetadataOutput?.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput?.metadataObjectTypes = [.qr]
            
            configurePreviewLayer(with: captureSession!)
        } catch {
            showAlertForcameraError(error: error as NSError)
        }
    }
    func configurePreviewLayer(with captureSession: AVCaptureSession){
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer?.videoGravity = .resizeAspectFill
        previewLayer?.frame = cameraView.layer.bounds
        cameraView.layer.addSublayer(previewLayer!)
    }
    func startRunning() {
        if self.captureSession?.isRunning == false{
            self.captureSession?.startRunning()
        }
        self.captureMetadataOutput?.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
    }
    func stopRunning() {
        previewLayer?.removeFromSuperlayer()
        dimmingLayer?.removeFromSuperlayer()
        maskingLayer?.removeFromSuperlayer()
        toggleFlashlight(with: false)
        loadingView.backgroundColor = .black
        captureMetadataOutput = nil
        captureSession?.stopRunning()
        captureSession = nil
    }
}

// MARK: - AVCaptureMetadataOutputObjects Delegate
extension ScannerViewController: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        let QRMetadataObjects = metadataObjects.filter{ $0.type == .qr}
        for qr in QRMetadataObjects{
            guard let codeObject  = qr as? AVMetadataMachineReadableCodeObject,
                let QRCodeValue = codeObject.stringValue else {return}
            
            audioPlayer.playDetectedSound(handler: {
                self.stopRunning()
                self.QRCodeString = QRCodeValue
                self.performSegue(withIdentifier: Constants.SegueResultContainer, sender: nil)
            })
            break
        }
    }
}

// MARK: - UIImagePickerControllerDelegate
extension ScannerViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            showAlert(alertTitle: "Error", alertMessage: "An internal error occurred. Couldn't get photo from Photo Library.".localized, cancelText: "Dismiss".localized)
            return
        }
        dismiss(animated: true, completion: {
            self.didFinishSelectPhotoFromLibrary(withImage: pickedImage)
        })
    }
}

// MARK: - ScannerPhotoSelectable
extension ScannerViewController: ScannerPhotoSelectable{
    func didFinishSelectPhotoFromLibrary(withImage image: UIImage) {
        decodeQRCode(sourceImage: image) { (success, qr_String) in
            self.audioPlayer.playDetectedSound {
                if success{
                    self.QRCodeString = qr_String
                    self.performSegue(withIdentifier: Constants.SegueResultContainer, sender: nil)
                }else{
                    
//                    self.showAlert(alertTitle: "Error", alertMessage: QRCodeError.NSealErrorCodePayloadSignatureMissing.localizedDescription, cancelText: "Dismiss".localized) {
//                        self.startRunning()
//                    }
                    
                    let errorMessage = "This certificate is either invalid or the QR Code is old version. Please convert to the new electronic certificate. More info at bit.ly/registrasilpjk".localized
                    let alert = UIAlertController(title: "Error".localized, message: errorMessage, preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        self.startRunning()
                    })
                    alert.addAction(okAction)
                    let moreInfoAction = UIAlertAction(title: "More Info".localized, style: .default, handler: { (action) in
                        self.startRunning()
                        if let url = URL(string: "https://bit.ly/registrasilpjk") {
                            UIApplication.shared.open(url)
                        }
                    })
                    alert.addAction(moreInfoAction)
                    self.present(alert, animated: true, completion: nil)
                    
                    
                }
            }
        }
    }
    
    func decodeQRCode(sourceImage: UIImage, handler: (Bool, String?) -> Void) {
        let source = CIImage(image: sourceImage)!
        let detector = CIDetector(ofType: CIDetectorTypeQRCode, context: nil, options: [CIDetectorAccuracy:CIDetectorAccuracyHigh])
        let features = detector?.features(in: source)
        for feature in features as! [CIQRCodeFeature] {
            guard let decoded = feature.messageString else {continue}
            handler(true, decoded)
            return
        }
        handler(false, nil)
    }
}

// MARK: - Utility Methods
extension ScannerViewController {
    func showAlertForcameraError(error: NSError) {
        let message = error.localizedFailureReason != nil ? error.localizedFailureReason : error.localizedDescription
        showAlert(alertTitle: "Camera Error", alertMessage: message!, cancelText: "Dismiss".localized, cancelHandler: nil)
    }
    
    func checkCameraAuthorization() {
        guard AVCaptureDevice.authorizationStatus(for: .video) == .authorized else {
            AVCaptureDevice.requestAccess(for: .video) { authorized in
                authorized ? nil : self.showAlertForAppSetting()
            }
            return
        }
    }
    
    func showAlertForAppSetting(){
        showAlert(alertTitle: "Camera Error", alertMessage: "This app is not authorized to use camera.".localized, cancelText: "Settings".localized) {
            guard let appSettings = URL(string: UIApplicationOpenSettingsURLString + Bundle.main.bundleIdentifier!),
                UIApplication.shared.canOpenURL(appSettings) else { return }
            UIApplication.shared.open(appSettings)
        }
    }
}
