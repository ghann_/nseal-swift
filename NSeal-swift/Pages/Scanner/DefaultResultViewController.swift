//
//  DefaultResultViewController.swift
//  NSeal-swift
//
//  Created by ma.ghani on 3/2/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import UIKit
import WebKit

class DefaultResultViewController: UIViewController {
    
    var nSealResult: NSealResult?
    
    fileprivate let nSealService = NSealService.shared
    fileprivate var webView: WKWebView!
    
    @IBOutlet weak var webViewContainer: UIView!
    
    func loadHTMLCertificate(of result: NSealResult) {
        nSealService.loadHTMLResult(of: result, handler: { success, HTMLResult, errMessage in
            if success{
                print("\n\(HTMLResult!)")
                self.webView.loadHTMLString(HTMLResult!, baseURL: nil)
            }else{
                self.showAlert(alertTitle: "Error", alertMessage: "\(errMessage!)", cancelHandler: {
                    self.navigationController?.popViewController(animated: true)
                })
            }
        })
    }
    
    func addWebViewConstrains() {
        webViewContainer.addConstraintsToSuperview(vc: self, superView: view)
        webView.addConstraints(toView: webViewContainer, leadingConst: 10, topConst: 5, trailingConst: 10, bottomConst: 56)
    }
}

// MARK: - Life Cycle
extension DefaultResultViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        loadHTMLCertificate(of: nSealResult!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        showLargeTitle(enable: false, toolBarHidden: false)
    }
}

// MARK: - WKUIDelegate
extension DefaultResultViewController: WKUIDelegate{
    override func loadView() {
        super.loadView()
        
        let webConfiguration = WKWebViewConfiguration()
        let customFrame = CGRect(origin: CGPoint.zero, size: CGSize(width: webViewContainer.frame.size.width, height: webViewContainer.frame.size.height))
        webView = WKWebView (frame: customFrame , configuration: webConfiguration)
        webView.contentMode = .scaleAspectFit
        webViewContainer.addSubview(webView)
        webView.uiDelegate = self
        webView.scrollView.showsVerticalScrollIndicator = false
        addWebViewConstrains()
    }
}
