//
//  CustomResultViewController.swift
//  NSeal-swift
//
//  Created by ma.ghani on 3/28/18.
//  Copyright © 2018 MiraiApps. All rights reserved.
//

import UIKit

class CustomResultViewController: UIViewController {

    var certificate: Certificate?
    
    fileprivate var infoBtnTapped: Bool = false
    
    @IBOutlet weak var tableVerify: UITableView!
    @IBOutlet weak var lblVerifySuccess: UILabel!
    @IBOutlet weak var lblVerifyInfo: UILabel!
    @IBOutlet weak var viewFixedHeaderContainer: UIView!
    @IBOutlet weak var viewTableContainer: UIView!
    @IBOutlet weak var lblCertificateTitle: UILabel!
    @IBOutlet weak var btnInfo: UIButton!
    @IBAction func btnInfoTapped(_ sender: UIButton!){
        /*
        infoBtnTapped = !infoBtnTapped
        if certificate?.type == .SBU{
            tableVerify.reloadSections([0, 15], with: .fade)
        }
        if certificate?.type == .SKA{
            tableVerify.reloadSections([0, 2, 5, 12], with: .fade)
        }
        UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
            sender.backgroundColor = self.infoBtnTapped ? Globals.Colors.unselectedTab : Globals.Colors.defaultBlue
            self.infoBtnTapped ? sender.setImage(#imageLiteral(resourceName: "infoTapped"), for: .normal) : sender.setImage(#imageLiteral(resourceName: "info"), for: .normal)
        })
        tableVerify.scrollToRow(at: IndexPath(row: 0, section: 0), at: .bottom, animated: true)
         */
    }
    
    func configureView() {
        viewTableContainer.addShadow()
        viewFixedHeaderContainer.addShadow()
        lblCertificateTitle.text = certificate?.type.title
        showLargeTitle(enable: false, toolBarHidden: false)
    }
    
    func configureVerifyTable() {
        tableVerify.backgroundColor = .white
        tableVerify.contentInset = UIEdgeInsetsMake(-36, 0, 56, 0)
        tableVerify.register(UINib(nibName: "RincianKompetensiKerjaCell", bundle: nil), forCellReuseIdentifier: "RincianKompetensiKerjaCell")
        tableVerify.register(UINib(nibName: "RincianSubKlasifikasiCell", bundle: nil), forCellReuseIdentifier: "RincianSubKlasifikasiCell")
        tableVerify.register(UINib(nibName: "RincianPernyataanCell", bundle: nil), forCellReuseIdentifier: "RincianPernyataanCell")
    }
    
    func configureVerifyLabel() {
        lblVerifySuccess.text = "Verify Successfully!".localized
        lblVerifyInfo.text = "Please compare the green text with the physical document".localized
    }
}

// MARK: - Life Cycle
extension CustomResultViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureVerifyTable()
        configureVerifyLabel()
    }
}

// MARK: - TableView DataSource
extension CustomResultViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return certificate!.type.numberOfRowHeaderTitlesWithTopHeader
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch certificate!.type {
        case .SKA1:
            return 1
        case .SBU1:
            return section == certificate?.type.numberOfRowHeaderTitles ? certificate!.subKualifikasiBU.count : 1
        case .SKT1:
            return section == certificate?.type.numberOfRowHeaderTitles ? certificate!.kompetensiKerja.count : 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        cell.backgroundColor = .white
        cell.selectionStyle = .none
        
        if indexPath.section == 0 {
            let defaultCell = tableView.dequeueReusableCell(withIdentifier: "DefaultCertificateCell", for: indexPath) as! DefaultCertificateCell
            cell = defaultCell.defaultInfoCell(withText: certificate!.type.infoMessage)
            return cell
        }else{
            switch certificate!.type {
            case .SKA1:
                switch indexPath.section{
                case 1...11:
                    let defaultCell = tableView.dequeueReusableCell(withIdentifier: "DefaultCertificateCell", for: indexPath) as! DefaultCertificateCell
                    switch indexPath.section {
                    case 1: cell = defaultCell.defaultCell(withText: certificate!.nama)
                    case 2: cell = defaultCell.defaultInfoCell(withText: "dinyatakan memiliki kompetensi dan kemampuan serta dapat melaksanakan kegiatan profesi konstruksi di seluruh wilayah Republik Indonesia, sebagai:")
                    case 3: cell = defaultCell.defaultCell(withText: certificate!.subKualifikasi)
                    case 4: cell = defaultCell.defaultCell(withText: certificate!.nomorRegistrasi)
                    case 5: cell = defaultCell.defaultCell(withText: "") // defaultCell.defaultInfoCell(withText: "Sertifikat ini berlaku paling lama 3 (tiga) tahun terhitung sejak ditetapkan.")
                    case 6: cell = defaultCell.defaultCell(withText: certificate!.tempatDitetapkan)
                    case 7: cell = defaultCell.defaultCell(withText: certificate!.tanggalDitetapkan)
                    case 8: cell = defaultCell.defaultCell(withText: certificate!.jabatanPenandatangan)
                    case 9: cell = defaultCell.defaultCell(withText: certificate!.namaPenandatangan)
                    case 10: cell = defaultCell.defaultCell(withText: certificate!.asosiasi)
                    case 11: cell = defaultCell.defaultCell(withText: certificate!.nomorBeritaAsosiasi)
                    default: ()
                    }
                case 12:
                    let pernyataanCell = tableView.dequeueReusableCell(withIdentifier: "RincianPernyataanCell", for: indexPath) as! RincianPernyataanCell
                    cell = pernyataanCell
                default: ()
                }
            case .SBU1:
                switch indexPath.section {
                case 1...22:
                    let defaultCell = tableView.dequeueReusableCell(withIdentifier: "DefaultCertificateCell", for: indexPath) as! DefaultCertificateCell
                    switch indexPath.section{
                    case 1: cell = defaultCell.defaultCell(withText: certificate!.namaBadanUsaha)
                    case 2: cell = defaultCell.defaultCell(withText: certificate!.namaPenanggungJawab)
                    case 3: cell = defaultCell.defaultCell(withText: certificate!.alamat)
                    case 4: cell = defaultCell.defaultCell(withText: certificate!.kabupaten)
                    case 5: cell = defaultCell.defaultCell(withText: certificate!.provinsi)
                    case 6: cell = defaultCell.defaultCell(withText: certificate!.nomorTelepon)
                    case 7: cell = defaultCell.defaultCell(withText: certificate!.email)
                    case 8: cell = defaultCell.defaultCell(withText: certificate!.npwp)
                    case 9: cell = defaultCell.defaultCell(withText: certificate!.jenisBadanUsaha)
                    case 10:cell = defaultCell.defaultCell(withText: certificate!.kategoriBadanUsaha)
                    case 11:cell = defaultCell.defaultCell(withText: certificate!.kekayaanBersih)
                    case 12:cell = defaultCell.defaultCell(withText: certificate!.kodePos)
                    case 13:cell = defaultCell.defaultCell(withText: certificate!.nomorFax)
                    case 14:cell = defaultCell.defaultCell(withText: certificate!.nomorRegistrasi)
                    case 15:cell = defaultCell.defaultInfoCell(withText: certificate!.type.extraInfoMessage)
                    case 16:cell = defaultCell.defaultCell(withText: certificate!.tempatDitetapkan)
                    case 17:cell = defaultCell.defaultCell(withText: certificate!.tanggalDitetapkan)
                    case 18:cell = defaultCell.defaultCell(withText: certificate!.jabatanPenandatangan)
                    case 19:cell = defaultCell.defaultCell(withText: certificate!.namaPenandatangan)
                    case 20:cell = defaultCell.defaultCell(withText: certificate!.klasifikasiSBU)
                    case 21:cell = defaultCell.defaultCell(withText: certificate!.golonganSBU)
                    case 22:cell = defaultCell.defaultCell(withText: certificate!.asosiasi)
                    default: ()
                    }
                case 23:
                    let subKlasifikasiCell = tableView.dequeueReusableCell(withIdentifier: "RincianSubKlasifikasiCell", for: indexPath) as! RincianSubKlasifikasiCell
                    subKlasifikasiCell.lblNumber.text = "\(certificate!.subKualifikasiBU[indexPath.row].nomorUrut)."
                    subKlasifikasiCell.lblSubklasifikasi.text = certificate!.subKualifikasiBU[indexPath.row].kualifikasi
                    subKlasifikasiCell.lblKodeSubklasifikasi.text = certificate!.subKualifikasiBU[indexPath.row].kodeSubklasifikasi
                    subKlasifikasiCell.lblDeskripsiSubklasifikasi.text = certificate!.subKualifikasiBU[indexPath.row].deskripsiSubkualifikasi
                    subKlasifikasiCell.lblTahunKD.text = certificate!.subKualifikasiBU[indexPath.row].tahunKD
                    subKlasifikasiCell.lblNilaiKD.text = certificate!.subKualifikasiBU[indexPath.row].nilaiKD
                    cell = subKlasifikasiCell
                default: ()
                }
            case .SKT1:
                switch indexPath.section {
                case 1...10:
                     let defaultCell = tableView.dequeueReusableCell(withIdentifier: "DefaultCertificateCell", for: indexPath) as! DefaultCertificateCell
                     switch indexPath.section{
                     case 1: cell = defaultCell.defaultCell(withText: certificate!.nama)
                     case 2: cell = defaultCell.defaultCell(withText: certificate!.alamat)
                     case 3: cell = defaultCell.defaultCell(withText: certificate!.subBidang)
                     case 4: cell = defaultCell.defaultCell(withText: certificate!.kualifikasi)
                     case 5: cell = defaultCell.defaultCell(withText: certificate!.tempatDitetapkan)
                     case 6: cell = defaultCell.defaultCell(withText: certificate!.tanggalDitetapkan)
                     case 7: cell = defaultCell.defaultCell(withText: certificate!.jabatanPenandatangan)
                     case 8: cell = defaultCell.defaultCell(withText: certificate!.namaPenandatangan)
                     case 9: cell = defaultCell.defaultCell(withText: certificate!.nomorRegistrasi)
                     case 10:cell = defaultCell.defaultCell(withText: certificate!.asosiasi)
                     default: ()
                    }
                case 11:
                    let kompetensiKerjaCell = tableView.dequeueReusableCell(withIdentifier: "RincianKompetensiKerjaCell", for: indexPath) as! RincianKompetensiKerjaCell
                    kompetensiKerjaCell.lblNumber.text = "\(certificate!.kompetensiKerja[indexPath.row].nomorUrut)."
                    kompetensiKerjaCell.lblKompetensiKerjaDesc.text = certificate!.kompetensiKerja[indexPath.row].deskripsi
                    cell = kompetensiKerjaCell
                default: ()
                }
            }
        }
        
        return cell
    }
}

// MARK: - TableView Delegate
extension CustomResultViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let normalHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 15))
        normalHeaderView.backgroundColor = .white
        let titleLabel = UILabel(frame: .zero)
        titleLabel.font = UIFont.systemFont(ofSize: 10, weight: .regular)
        titleLabel.textColor = Constants.Colors.defaultBlack
        titleLabel.text = self.tableView(tableView, titleForHeaderInSection: section)
        titleLabel.sizeToFit()
        
        normalHeaderView.addSubview(titleLabel)
        titleLabel.addConstraints(toView: normalHeaderView, leadingConst: 16, topConst: 5, trailingConst:0, bottomConst: -10)
        
        switch certificate!.type {
        case .SBU1:
            switch section{
            case 0, 15: return nil
            default: return normalHeaderView
            }
        case .SKA1:
            switch section{
            case 0, 2, 12: return nil
            default: return normalHeaderView
            }
        default:
            return section == 0 ? nil : normalHeaderView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if certificate!.type == .SKA1 {
            switch section{
            case 0: return 0
            case 2, 5, 12: return .leastNormalMagnitude
            default: return 15
            }
        }else if certificate!.type == .SBU1{
            switch section{
            case 0: return 0
            case 15: return .leastNormalMagnitude
            default: return 15
            }
        }else{
          return section == 0 ? 0 : 15
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? nil : certificate?.type.listOfRowHeaderTitles[section - 1]
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return infoBtnTapped ? UITableViewAutomaticDimension : 0
        case 2 where certificate?.type == .SKA1:
            return infoBtnTapped ? UITableViewAutomaticDimension : 0
        case 5 where certificate?.type == .SKA1:
            return infoBtnTapped ? UITableViewAutomaticDimension : 0
        case 12 where certificate?.type == .SKA1:
            return infoBtnTapped ? UITableViewAutomaticDimension : 0
        case 15 where certificate?.type == .SBU1:
            return infoBtnTapped ? UITableViewAutomaticDimension : 0
        default:
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == certificate?.type.numberOfRowHeaderTitles ? UITableViewAutomaticDimension : 40
    }
}
