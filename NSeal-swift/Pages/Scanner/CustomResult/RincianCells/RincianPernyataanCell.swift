//
//  RincianPernyataanCell.swift
//  NSeal-swift
//
//  Created by ma.ghani on 4/4/18.
//  Copyright © 2018 MiraiApps. All rights reserved.
//

import UIKit

class RincianPernyataanCell: UITableViewCell {

    @IBOutlet weak var lblPernyataan: UILabel!{
        didSet{
            let attributedString = NSMutableAttributedString(string: "PERNYATAAN\nSaya yang bertanda tangan di bawah ini berjanji:\n1. Akan patuh melaksanakan Kode Etik Asosiasi di mana saya menjadi anggotanya.\n2. Akan mematuhi segala ketentuan hukum yang sah berlaku di tempat dilaksanakannya karya saya.\n\nDengan ini saya menyatakan bahwa :\nSaya :\na. mengakui dan menerima sepenuhnya wewenang Asosiasi Profesi di mana saya menjadi anggotanya untuk menilai pengaduan dan atau keluhan apapun dari masyarakat yang menyangkut janji tersebut di atas.\nb. menerima sanksi apapun apabila saya melanggar janji tersebut.", attributes: [
                .font: UIFont.systemFont(ofSize: 14.0, weight: .regular),
                .foregroundColor: UIColor(white: 54.0 / 255.0, alpha: 1.0),
                .kern: 0.17
                ])
            attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 14.0, weight: .bold), range: NSRange(location: 0, length: 10))
            lblPernyataan.attributedText = attributedString
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
    }
}
