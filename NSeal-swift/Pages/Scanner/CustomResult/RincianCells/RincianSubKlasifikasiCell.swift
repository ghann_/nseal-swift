//
//  RincianSubKlasifikasiCell.swift
//  NSeal-swift
//
//  Created by ma.ghani on 4/3/18.
//  Copyright © 2018 MiraiApps. All rights reserved.
//

import UIKit

class RincianSubKlasifikasiCell: UITableViewCell {

    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblSubklasifikasi: UILabel!
    @IBOutlet weak var lblKodeSubklasifikasi: UILabel!
    @IBOutlet weak var lblDeskripsiSubklasifikasi: UILabel!
    @IBOutlet weak var lblTahunKD: UILabel!
    @IBOutlet weak var lblNilaiKD: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
    }
}
