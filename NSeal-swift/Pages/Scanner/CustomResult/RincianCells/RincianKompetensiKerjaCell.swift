//
//  RincianKompetensiKerjaCell.swift
//  NSeal-swift
//
//  Created by ma.ghani on 4/3/18.
//  Copyright © 2018 MiraiApps. All rights reserved.
//

import UIKit

class RincianKompetensiKerjaCell: UITableViewCell {

    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblKompetensiKerjaDesc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
    }
}
