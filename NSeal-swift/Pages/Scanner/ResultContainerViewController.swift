//
//  ResultContainerViewController.swift
//  NSeal-swift
//
//  Created by ma.ghani on 4/5/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import UIKit
import SafariServices
 
class ResultContainerViewController: UIViewController {
    
    var qrCodeString: String = ""
    var resultOrigin: ResultOrigin = .create
    
    fileprivate var validationDetailURL: URL?
    fileprivate var validationView: ValidationView?
    fileprivate var nSealResult: NSealResult = NSealResult()
    fileprivate var certificate: Certificate?
    fileprivate var displayResult: CertificateDisplayResult = .default
    fileprivate let nSealService = NSealService.shared
    fileprivate var userSettings: UserSettings? {
        guard let settings =  UserSettings.getUserSettings() else{
            showAlert(alertTitle: "Error", alertMessage: "An internal error occurred. Couldn't get user settings.".localized)
            return nil
        }
        return settings
    }
    
    fileprivate lazy var defaultResultController: UIViewController? = {
        let defaultResultVC = storyboard?.instantiateViewController(withIdentifier: "DefaultResult") as! DefaultResultViewController
        defaultResultVC.nSealResult = nSealResult
        return defaultResultVC
    }()
    
    fileprivate lazy var customResultViewController: UIViewController? = {
        let customResultVC = storyboard?.instantiateViewController(withIdentifier: "CustomResult") as! CustomResultViewController
        customResultVC.certificate = certificate
        return customResultVC
    }()
    
    fileprivate lazy var titleView: UIView = {
        let viewTitle = UIView(frame: CGRect(x: 0, y: 0, width: 150, height: 44))
        let lblScanned = UILabel(frame: CGRect(x: 0, y: 4, width: 150, height: 13))
        lblScanned.font = UIFont.systemFont(ofSize: 11)
        lblScanned.textAlignment = .center
        let lblDate = UILabel(frame: CGRect(x: 0, y: lblScanned.frame.maxY, width: 150, height: 20))
        lblDate.font = UIFont.systemFont(ofSize: 13, weight: .medium)
        lblDate.textAlignment = .center
        
        switch resultOrigin {
        case .view(let id):
            lblScanned.text = "Scanned on".localized
            lblDate.text = "\(History.getHistoryForId(historyId: id)?.date.withoutTime ?? "")"
        default: ()
        }
        
        viewTitle.addSubview(lblScanned)
        viewTitle.addSubview(lblDate)
        
        return viewTitle
    }()
    
    fileprivate lazy var barButtons: [UIBarButtonItem] = {
        var items = [UIBarButtonItem]()
        let trashButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(btnRemoveTapped))
        trashButton.tintColor = Constants.Colors.defaultBlue
        let shareButton = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(btnShareTapped))
        shareButton.tintColor = Constants.Colors.defaultBlue
        
        let segmentedButton = CertificateResultSegmentedControl(items: nil)
        segmentedButton.delegate = self
        let segmentedBarButton = UIBarButtonItem(customView: segmentedButton)
        
        // For version 1.1: Hide Default Result first
        //items.append(UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil))
        //items.append(UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil))
        //items.append(segmentedBarButton)
        items.append(UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil))
        items.append(trashButton)
        
        return items
    }()
    
    @IBOutlet weak var resultContainerVew: UIView!
    
    func configureView() {
        navigationItem.titleView = titleView
        toolbarItems = barButtons
        navigationController?.toolbar.clipsToBounds = true
        tabBarController?.tabBar.isHidden = true
    }
    
    func getCertificate(ofCertificate type: CertificateType) {
        guard let displayDataList = nSealResult.displayDataList as? [NSealDisplayData] else{
            showAlert(alertTitle: "Error", alertMessage: "An internal error occurred. Couldn't get display data.".localized)
            return
        }
        
        switch type {
        case .SKA1:
            Certificate.getSKACertificate(withDisplayData: displayDataList) { success, ska, error in
                if success {
                    certificate = ska
                    saveToHistory(of: nSealResult, certificateType: type)
                    displayCurrentTab(result: displayResult)
                    checkValidation(with: .SKA1)
                }else{
                    showAlert(alertTitle: "Error", alertMessage: error!, cancelText: "Dismiss".localized){
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        case .SKT1:
            Certificate.getSKTCertificate(withDisplayData: displayDataList) { success, skt, error in
                if success {
                    certificate = skt
                    saveToHistory(of: nSealResult, certificateType: type)
                    displayCurrentTab(result: displayResult)
                    checkValidation(with: .SKT1)
                }else{
                    showAlert(alertTitle: "Error", alertMessage: error!, cancelText: "Dismiss".localized){
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        case .SBU1:
            Certificate.getSBUCertificate(withDisplayData: displayDataList) { success, sbu, error in
                if success {
                    certificate = sbu
                    saveToHistory(of: nSealResult, certificateType: type)
                    displayCurrentTab(result: displayResult)
                    checkValidation(with: .SBU1)
                }else{
                    showAlert(alertTitle: "Error", alertMessage: error!, cancelText: "Dismiss".localized){
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
    }
    
    func saveToHistory(of result: NSealResult, certificateType type: CertificateType) {
        switch resultOrigin{
        case .create:
            History.createHistory(scanDate: Date().today, scanTitle: type == .SBU1 ? certificate!.namaBadanUsaha : certificate!.nama,
                                  scanDescription: type.titleSimplified, qrString: qrCodeString, validValue: nSealResult.isValid)
        default: ()
        }
    }
    
    func resultViewControllerForSegmented(result type: CertificateDisplayResult) -> UIViewController? {
        // For version 1.1: Hide Default Result first
        // return type == .default ? defaultResultController : customResultViewController
        return customResultViewController
    }
    
    func displayCurrentTab(result type: CertificateDisplayResult) {
        guard let vc = resultViewControllerForSegmented(result: type) else {return}
        addChildViewController(vc)
        vc.didMove(toParentViewController: self)
        vc.view.frame = resultContainerVew.bounds
        resultContainerVew.addSubview(vc.view)
    }
    
    func getQRResult() {
        nSealService.readQRCode(qrCodeString: qrCodeString) { success, nSealResult, errMessage in
            if success{
                guard let certificateType = CertificateType.getType(rawString: nSealResult!.templateID) else {
                    self.presentOldVersionError()
                    return
                }
                self.nSealResult = nSealResult!
                self.getCertificate(ofCertificate: certificateType)
            }else{
                self.presentOldVersionError()
            }
        }
    }
    
    func presentOldVersionError() {
        
        let errorMessage = "This certificate is either invalid or the QR Code is old version. Please convert to the new electronic certificate. More info at bit.ly/registrasilpjk".localized
        let alert = UIAlertController(title: "Error".localized, message: errorMessage, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.navigationController?.popViewController(animated: true)
        })
        alert.addAction(okAction)
        let moreInfoAction = UIAlertAction(title: "More Info".localized, style: .default, handler: { (action) in
            self.navigationController?.popViewController(animated: true)
            if let url = URL(string: "https://bit.ly/registrasilpjk") {
                UIApplication.shared.open(url)
            }
        })
        alert.addAction(moreInfoAction)
        self.present(alert, animated: true, completion: nil)
        
    }

    func addValidationView(){
        validationView = ValidationView()
        view.addSubview(validationView!)
        validationView?.delegate = self
        validationView?.addConstraints(vc: self, superView: view, leadingConst: 0, topConst: nil, trailingConst: 0, bottomConst: 0, widthConst: view.frame.width, heightConst: 56)
    }
    
    func checkValidation(with type: CertificateType){
        Validation.isValid(certificateType: type, uniqueID: certificate!.idUnikPersonal, nsealID: nSealResult.sealID) { result in
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(2000)) {
                switch result {
                case .success(let validation, let detailURL):
                    self.validationDetailURL = detailURL
                    self.validationView?.configureView(to: validation.valid == "0" ? .isRevoked : .isValid)
                    self.changeCertificateStatus(isValid: validation.valid == "0" ? false : true)
                case .failure(let error, let detailURL):
                    print(error)
                    self.validationDetailURL = detailURL
                    self.validationView!.configureView(to: .notConnected)
                }
            }
        }
    }
    
    func changeCertificateStatus(isValid value: Bool) {
        switch resultOrigin {
        case .create:
            History.changeCertificateStatus(toID: nil, isValid: value)
        case .view(let id):
            History.changeCertificateStatus(toID: id, isValid: value)
        }
    }
    
    @objc func btnShareTapped() {
        /*
         webView.getCertificateImage { image in
         let activity = UIActivityViewController(activityItems: [image], applicationActivities: nil)
         activity.completionWithItemsHandler = { type, completed, returned, error in
         if completed && type == .saveToCameraRoll{
         self.showAlert(alertTitle: "Success".localized, alertMessage: "Certificate image saved to camera roll.".localized, cancelText: "Dismiss".localized)
         }
         }
         self.present(activity, animated: true, completion: nil)
         }
         */
    }
    @objc func btnRemoveTapped() {
        showAlert(style: .actionSheet, alertTitle: "Remove Certificate".localized, alertMessage: "Remove this certificate from history?".localized,
                  okText: "Remove".localized, okHandler: {
                    var certificateId: Int64 = -1
                    switch self.resultOrigin{
                    case .create:
                        certificateId = History.getLastCertificateId()
                    case .view(let certId):
                        certificateId = certId
                    }
                    History.removeHistory(forId: certificateId){ success in
                        let title = success ? "Success" : "Error"
                        let message = success ? "Certificate Removed" : "An internal error occurred. Unexpected error."
                        self.showAlert(alertTitle: title.localized, alertMessage: message.localized, cancelText: "Dismiss".localized, cancelHandler: {
                            self.navigationController?.popViewController(animated: true)
                        })
                    }
        }, cancelHandler: nil)
    }
}

// MARK: - Life Cycle
extension ResultContainerViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        addValidationView()
        displayResult = userSettings!.certificateDisplayResult
        getQRResult()
        resultContainerVew.addConstraintsToSuperview(vc: self, superView: view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        showLargeTitle(enable: false, toolBarHidden: false)
        configureView()
    }
}

// MARK: - ValidationViewDelegate
extension ResultContainerViewController: ValidationViewDelegate {
    func didTapViewDetails() {
        if let url = validationDetailURL {
            let vc = SFSafariViewController(url: url)            
            present(vc, animated: true)
        }
    }
    
    func didTapRetry() {
        DispatchQueue.main.async {
            self.validationView?.configureView(to: .isChecking)
            self.checkValidation(with: self.certificate!.type)
        }
    }
}

// MARK: - SegmentedResultDelegate
extension ResultContainerViewController: SegmentedResultDelegate {
    func didChangeValue(displayResult: CertificateDisplayResult) {
        animateContainerView(displayResult: displayResult)
        UserSettings.saveDisplayResult(certificateDisplayResultString: displayResult.rawValue)
    }
    
    func animateContainerView(displayResult: CertificateDisplayResult) {
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveEaseOut, animations: {
            self.resultContainerVew.alpha = 0
        }) { _ in
            UIView.animate(withDuration: 0.15, delay: 0, options: .curveEaseOut, animations: {
                self.displayCurrentTab(result: displayResult)
                UIView.animate(withDuration: 0.15, animations: {
                    self.resultContainerVew.alpha = 1
                }, completion: nil)
            }, completion: nil)
        }
    }
}
