//
//  ValidationView.swift
//  NSeal-swift
//
//  Created by ma.ghani on 5/25/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import Foundation

protocol ValidationViewDelegate: class {
    func didTapViewDetails()
    func didTapRetry()
}

class ValidationView: UIView{
    
    weak var delegate: ValidationViewDelegate?
    
    fileprivate lazy var lblDescription: UILabel = {
        let lblDescription = UILabel(frame: .zero)
        lblDescription.textColor = Constants.Colors.defaultBlack
        lblDescription.translatesAutoresizingMaskIntoConstraints = false
        return lblDescription
    }()
    fileprivate lazy var actvIndicator: UIActivityIndicatorView = {
        let actvIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        actvIndicator.color = Constants.Colors.defaultBlue
        actvIndicator.translatesAutoresizingMaskIntoConstraints = false
        actvIndicator.startAnimating()
        return actvIndicator
    }()
    fileprivate lazy var btnDetail: UIButton = {
        let btnDetail = UIButton(type: .system)
        btnDetail.titleLabel?.font = .systemFont(ofSize: 14, weight: .bold)
        btnDetail.titleLabel?.textColor = .white
        btnDetail.tintColor = .white
        btnDetail.backgroundColor = Constants.Colors.defaultBlue
        btnDetail.layer.cornerRadius = 10
        btnDetail.setTitle("VIEW DETAILS".localized, for: .normal)
        btnDetail.alpha = 0
        btnDetail.translatesAutoresizingMaskIntoConstraints = false
        btnDetail.addTarget(self, action: #selector(btnDetailTapped), for: .touchUpInside)
        return btnDetail
    }()
    fileprivate lazy var btnRetry: UIButton = {
        let btnDetail = UIButton(type: .system)
        btnDetail.titleLabel?.font = .systemFont(ofSize: 14, weight: .bold)
        btnDetail.titleLabel?.textColor = .white
        btnDetail.tintColor = .white
        btnDetail.backgroundColor = Constants.Colors.btnRetry
        btnDetail.layer.cornerRadius = 10
        btnDetail.setTitle("RETRY".localized, for: .normal)
        btnDetail.alpha = 0
        btnDetail.translatesAutoresizingMaskIntoConstraints = false
        btnDetail.addTarget(self, action: #selector(btnRetryTapped), for: .touchUpInside)
        return btnDetail
    }()
    
    @objc func btnDetailTapped() {
        delegate?.didTapViewDetails()
    }
    @objc func btnRetryTapped() {
        delegate?.didTapRetry()
    }
    
    enum ValidationState: String {
        case isChecking, isValid, isRevoked, notConnected
        
        var backgroundColor: UIColor{
            switch self {
            case .isChecking:   return Constants.Colors.checking
            case .isValid:      return Constants.Colors.certValid
            case .isRevoked:    return Constants.Colors.certInvalid
            case .notConnected: return Constants.Colors.notConnected
            }
        }
        
        var description: NSMutableAttributedString {
            switch self {
            case .isChecking:
                let checkingCertificate = NSMutableAttributedString(string: "Checking to server...".localized, attributes: [
                    .font: UIFont.systemFont(ofSize: 14.0, weight: .regular),
                    .foregroundColor: UIColor(white: 1.0, alpha: 1.0)
                    ])
                return checkingCertificate
            case .isValid:
                let validCertificate = NSMutableAttributedString(string: "Certificate ".localized, attributes: [
                    .font: UIFont.systemFont(ofSize: 14.0, weight: .regular),
                    .foregroundColor: UIColor(white: 1.0, alpha: 1.0)
                    ])
                let valid = NSMutableAttributedString(string: "is VALID".localized, attributes: [
                    .font: UIFont.systemFont(ofSize: 14.0, weight: .bold),
                    .foregroundColor: UIColor(white: 1.0, alpha: 1.0)
                    ])
                validCertificate.append(valid)
                return validCertificate
            case .isRevoked:
                let certificateis = NSMutableAttributedString(string: "Certificate is ".localized, attributes: [
                    .font: UIFont.systemFont(ofSize: 14.0, weight: .regular),
                    .foregroundColor: UIColor(white: 1.0, alpha: 1.0)
                    ])
                let revoked = NSMutableAttributedString(string: "REVOKED".localized, attributes: [
                    .font: UIFont.systemFont(ofSize: 14.0, weight: .bold),
                    .foregroundColor: UIColor(white: 1.0, alpha: 1.0)
                    ])
                certificateis.append(revoked)
                return certificateis
            case .notConnected:
                let notConnected = NSMutableAttributedString(string: "Failed checking to server.".localized, attributes: [
                    .font: UIFont.systemFont(ofSize: 14.0, weight: .regular),
                    .foregroundColor: UIColor(white: 1.0, alpha: 1.0)
                    ])
                return notConnected
            }
        }
    }
    
    private func addidicatorView(){
        addSubview(actvIndicator)
        actvIndicator.addConstraints(toView: self, leadingConst: 0, topConst: 0, trailingConst: 0, bottomConst: 15)
    }
    
    private func addConstraints(){
        lblDescription.addConstraints(toView: self, leadingConst: 17, topConst: 20, trailingConst: 0, bottomConst: 20)
        btnDetail.heightAnchor.constraint(equalToConstant: 32).isActive = true
        btnDetail.widthAnchor.constraint(equalToConstant: 125).isActive = true
        btnDetail.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        btnDetail.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -17).isActive = true
        btnRetry.heightAnchor.constraint(equalToConstant: 32).isActive = true
        btnRetry.widthAnchor.constraint(equalToConstant: 125).isActive = true
        btnRetry.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        btnRetry.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -17).isActive = true
        actvIndicator.heightAnchor.constraint(equalToConstant: 20).isActive = true
        actvIndicator.widthAnchor.constraint(equalToConstant: 20).isActive = true
        actvIndicator.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        actvIndicator.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -17).isActive = true
    }
    
    private func toggleAlpha(of selectedView: UIView){
        UIView.animate(withDuration: 0.35, delay: 0, options: .curveEaseInOut, animations: {
            selectedView.alpha = selectedView.alpha == 0 ? 1 : 0
        }, completion: nil)
    }
    
    func configureView(to state: ValidationState){
        DispatchQueue.main.async {
            self.lblDescription.attributedText = state.description
            
            UIView.transition(with: self, duration: 0.35, options: .transitionCrossDissolve, animations: {
                self.backgroundColor = state.backgroundColor
            }, completion: nil)
            
            switch state {
            case .isChecking:
                self.btnRetry.alpha = 0
                self.actvIndicator.alpha = 1
                self.lblDescription.textColor = Constants.Colors.defaultBlack
            case .isValid, .isRevoked: ()
                self.btnRetry.alpha = 0
                self.toggleAlpha(of: self.actvIndicator)
                self.toggleAlpha(of: self.btnDetail)
                self.btnDetail.backgroundColor = state == .isRevoked ? Constants.Colors.btnRevoked : Constants.Colors.btnValid
            case .notConnected:
                self.actvIndicator.alpha = 0
                self.btnRetry.alpha = 1
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: 300, height: 56))
        addSubview(lblDescription)
        addSubview(btnDetail)
        addSubview(btnRetry)
        addSubview(actvIndicator)
        addConstraints()
        configureView(to: .isChecking)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
