//
//  CertificateResultSegmentedControl.swift
//  NSeal-swift
//
//  Created by ma.ghani on 4/5/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import UIKit

class CertificateResultSegmentedControl: UISegmentedControl {

    weak var delegate: SegmentedResultDelegate?
    fileprivate var userSettings: UserSettings? {
        guard let settings =  UserSettings.getUserSettings() else{
            print("An internal error occurred. Couldn't get user settings.".localized)
            return nil
        }
        return settings
    }
    
    override init(items: [Any]?) {
        super.init(items: ["Default", "Custom"])
        configureSegmentedControl()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureSegmentedControl()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureSegmentedControl(){
        self.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        self.tintColor = Constants.Colors.certValid
        self.selectedSegmentIndex = userSettings!.certificateDisplayResult == .default ? 0 : 1
        self.addTarget(self, action: #selector(valueChanged), for: .valueChanged)
    }
    
    @objc func valueChanged(){
        delegate?.didChangeValue(displayResult: selectedSegmentIndex == 0 ? .default : .custom)
    }
}
