//
//  InfoCell.swift
//  NSeal-swift
//
//  Created by ma.ghani on 3/7/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import UIKit

class InfoCell: UITableViewCell {

    @IBOutlet weak var lblInfo: UILabel!
    
}
