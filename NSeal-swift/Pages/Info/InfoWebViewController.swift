//
//  InfoWebViewController.swift
//  NSeal-swift
//
//  Created by ma.ghani on 4/21/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import UIKit
import WebKit

class InfoWebViewController: UIViewController {
    
    fileprivate var webView: WKWebView?
    fileprivate lazy var textViewAbout: UITextView = {
        let tvAbout = UITextView(frame: .zero)
        tvAbout.showsVerticalScrollIndicator = false
        tvAbout.isEditable = false
        tvAbout.isSelectable = false
        return tvAbout
    }()
    
    @IBOutlet weak var viewAbout: UIView!{
        didSet{ viewAbout.addConstraintsToSuperview(vc: self, superView: view) }
    }
    
    var appInformation: AppInformation = .about{
        didSet{
            switch appInformation {
            case .FAQ: title = "FAQ"
            case .TNC: title = "Terms & Conditions"
            case .about: title = "About"
            }
        }
    }
    
    func configureAboutTextView(){
        let aboutTextAttr: [NSAttributedStringKey : Any] = [.font: UIFont.systemFont(ofSize: 14.0, weight: .regular),
                                                            .foregroundColor: Constants.Colors.defaultBlack, .kern: 0.19]
        let aboutAttributedText = NSMutableAttributedString(string: "LPJK CERTIFICATE SCANNER\n", attributes: aboutTextAttr)
        
        textViewAbout.attributedText = aboutAttributedText
        viewAbout.addSubview(textViewAbout)
        textViewAbout.contentOffset = .zero
        textViewAbout.addConstraints(toView: viewAbout, leadingConst: 16, trailingConst: 16)
    }
}

// MARK: - WebView Delegate
extension InfoWebViewController: WKUIDelegate {
    override func loadView() {
        super.loadView()
        
        guard appInformation != .about else {
            configureAboutTextView()
            return
        }
        let customFrame = CGRect(origin: CGPoint.zero, size: CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height))
        webView = WKWebView (frame: customFrame , configuration: WKWebViewConfiguration())
        webView!.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        webView!.uiDelegate = self
        webView?.load(URLRequest(url: appInformation.url!))
        viewAbout.addSubview(webView!)
        webView?.addConstraints(toView: viewAbout)
    }
}
