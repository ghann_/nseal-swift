//
//  InfoViewController.swift
//  NSeal-swift
//
//  Created by ma.ghani on 3/7/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import UIKit
import MessageUI

class InfoViewController: UIViewController {
    
    fileprivate var selectedAppInformation: AppInformation = .about
    fileprivate var arrInfo: [String] {
        return [
            "About", "FAQ", "Terms & Conditions",
            "Contact Us", "Visit LPJK Website"
        ]
    }
    fileprivate lazy var footerView: UIView = {
        var tableViewHeight: CGFloat {
            infoTableView.layoutIfNeeded()
            return infoTableView.contentSize.height
        }
        
        let settingsTableSize = infoTableView.frame.size
        let rectLastRow = infoTableView.rectForRow(at: IndexPath(row: 5, section: 0))
        let lastRowFrame = infoTableView.convert(rectLastRow, to: view)
        let tabBarYPos = self.tabBarController!.tabBar.frame.origin.y
        let emptySpaceHeight = tabBarYPos - (tableViewHeight + lastRowFrame.size.height + 5)
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: settingsTableSize.width, height: emptySpaceHeight))
        let lblVersion = UILabel(frame: CGRect(x: 0, y: footerView.frame.maxY * 0.7, width: UIScreen.main.bounds.width, height: 14))
        lblVersion.textAlignment = .center
        lblVersion.font = UIFont.systemFont(ofSize: 14, weight: .light)
        lblVersion.textColor = Constants.Colors.unselectedTab
        lblVersion.text = "VERSION".localized
        
        let lblVNum = UILabel(frame: CGRect(x: 0, y: lblVersion.frame.maxY, width: UIScreen.main.bounds.width, height: 20))
        lblVNum.textAlignment = .center
        lblVNum.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        lblVNum.textColor = Constants.Colors.unselectedTab
        lblVNum.text = "\(Bundle.main.currentVersionNumber ?? "")"
        
        footerView.addSubview(lblVersion)
        footerView.addSubview(lblVNum)
        
        return footerView
    }()
    
    @IBOutlet weak var infoTableView: UITableView!
    
    func configureView() {
        infoTableView.reloadData()
        infoTableView.tableFooterView = footerView
        showLargeTitle(enable: true, withTitle: "Info".localized)
    }
    
    func getMailComposer() -> MFMailComposeViewController? {
        guard MFMailComposeViewController.canSendMail() else {
            showAlert(alertTitle: "Error", alertMessage: "Mail services are not available".localized)
            return nil
        }
        let composer = MFMailComposeViewController()
        composer.mailComposeDelegate = self
        composer.setSubject("LPJK Certificate Scanner")
        composer.setToRecipients(["sti@lpjk.net"])
        return composer
    }
    
    func sendEmailToLPJK() {
        guard let composer = getMailComposer() else {return}
        present(composer, animated: true, completion: nil)
    }
}

// MARK: - Life Cycle
extension InfoViewController{
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        configureView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.SegueAppInformation{
            guard let appInformationVC = segue.destination as? InfoWebViewController else {return}
            appInformationVC.appInformation = selectedAppInformation
        }
    }
}

// MARK: - TableView DataSource
extension InfoViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InfoCell", for: indexPath) as! InfoCell
        cell.lblInfo.text = arrInfo[indexPath.row].localized
        cell.accessoryType = indexPath.row < 3 ? .disclosureIndicator : .none
        if indexPath.row > 2{
            cell.lblInfo.textColor = Constants.Colors.defaultBlue
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
        }
        return cell
    }
}

// MARK: - TableView Delegate
extension InfoViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
        case 0: selectedAppInformation = .about
        case 1: selectedAppInformation = .FAQ
        case 2: selectedAppInformation = .TNC
        case 3: sendEmailToLPJK()
        case 4: UIApplication.shared.open(URL(string: "http://lpjk.net")!, options: [:], completionHandler: nil)
        default: ()
        }
        
        indexPath.row < 3 ? performSegue(withIdentifier: Constants.SegueAppInformation, sender: nil) : nil
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}

// MARK: - MFMailComposerDelegate
extension InfoViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
