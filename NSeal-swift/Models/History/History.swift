//
//  History.swift
//  NSeal-swift
//
//  Created by ma.ghani on 3/7/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import Foundation
import SQLite

struct History{
    var id: Int64
    var date: String
    var title: String
    var description: String
    var qr_string: String
    var isValid: Bool
    
    init(id: Int64, date: String, title: String, description: String, qr_string: String, isValid: Bool) {
        self.id = id
        self.date = date
        self.title = title
        self.description = description
        self.qr_string = qr_string
        self.isValid = isValid
    }
    
    static func createDatabaseTable() {
        do{
            let db = try Connection("\(Constants.databasePath)/db.sqlite3")
            let histories = Table("histories")
            let id = Expression<Int64>("id")
            let date = Expression<String>("date")
            let title = Expression<String>("title")
            let description = Expression<String>("description")
            let qr_string = Expression<String>("qr_string")
            let isValid = Expression<Bool>("isValid")
            
            try db.run(histories.create(ifNotExists: false) { t in
                t.column(id, primaryKey: .autoincrement)
                t.column(date)
                t.column(title)
                t.column(description)
                t.column(qr_string)
                t.column(isValid)
            })
        }catch{
            print("Error: \(error)")
            return
        }
    }
    
    static func getAllHistory() -> [History]? {
        do{
            let db = try Connection("\(Constants.databasePath)/db.sqlite3")
            let historiesTable = Table("histories")
            let id = Expression<Int64>("id")
            let date = Expression<String>("date")
            let title = Expression<String>("title")
            let description = Expression<String>("description")
            let qr_string = Expression<String>("qr_string")
            let isValid = Expression<Bool>("isValid")
            
            var histories: [History] = []
            for hstr in try db.prepare(historiesTable) {
                let history = History(id: hstr[id], date: hstr[date], title: hstr[title], description: hstr[description], qr_string: hstr[qr_string], isValid: hstr[isValid])
                histories.append(history)
            }
            
            return histories.reversed()
        }catch{
            print("Error: \(error)")
            return nil
        }
    }
    
    static func getHistoryForId(historyId: Int64) -> History? {
        do{
            let db = try Connection("\(Constants.databasePath)/db.sqlite3")
            let histories = Table("histories")
            let id = Expression<Int64>("id")
            let date = Expression<String>("date")
            let title = Expression<String>("title")
            let description = Expression<String>("description")
            let qr_string = Expression<String>("qr_string")
            let isValid = Expression<Bool>("isValid")
            
            let history = histories.where(id == historyId)
            for hstr in try db.prepare(history) {
                let history = History(id: hstr[id], date: hstr[date], title: hstr[title], description: hstr[description], qr_string: hstr[qr_string], isValid: hstr[isValid])
                return history
            }
        }catch{
            print("Error: \(error)")
            return nil
        }
        return nil
    }
    
    static func getLastCertificateId() -> Int64 {
        do{
            let db = try Connection("\(Constants.databasePath)/db.sqlite3")
            let histories = Table("histories")
            let id = Expression<Int64>("id")
            
            let lastRow = histories.order(id.desc).limit(1)
            if let history = try db.pluck(lastRow) {
                return history[id]
            }
        }catch{
            print("Error: \(error)")
            return -1
        }
        return -1
    }
    
    static func createHistory(scanDate: String, scanTitle: String, scanDescription: String, qrString: String, validValue: Bool) {
        do{
            let db = try Connection("\(Constants.databasePath)/db.sqlite3")
            let histories = Table("histories")
            let date = Expression<String>("date")
            let title = Expression<String>("title")
            let description = Expression<String>("description")
            let qr_string = Expression<String>("qr_string")
            let isValid = Expression<Bool>("isValid")
            
            let insert = histories.insert(date <- scanDate, title <- scanTitle, description <- scanDescription, qr_string <- qrString, isValid <- validValue)
            _ = try db.run(insert)
        }catch{
            print("Error: \(error)")
            return
        }
    }
    
    typealias RemoveHistoryHandler = ((Bool)->Void)?
    static func removeHistory(forId historyId: Int64, handler: RemoveHistoryHandler = nil) {
        do{
            let db = try Connection("\(Constants.databasePath)/db.sqlite3")
            let histories = Table("histories")
            let id = Expression<Int64>("id")
            let willDelete = histories.filter(id == historyId)
            
            if try db.run(willDelete.delete()) > 0 {
                print("History with id \(historyId) deleted.")
                handler?(true)
            } else {
                print("History with id \(historyId) not found.")
                handler?(false)
            }
        }catch{
            print("Error: \(error)")
            return
        }
    }
    
    static func dropTable(handler: (()->Void)? = nil) {
        do{
            let db = try Connection("\(Constants.databasePath)/db.sqlite3")
            let histories = Table("histories")
            
            try db.run(histories.delete())
            handler?()
        }catch{
            print("Error: \(error)")
            return
        }
    }
    
    static func changeCertificateStatus(toID: Int64?, isValid value: Bool) {
        do{
            let db = try Connection("\(Constants.databasePath)/db.sqlite3")
            let histories = Table("histories")
            let id = Expression<Int64>("id")
            let isValid = Expression<Bool>("isValid")

            let history = histories.filter(id == toID ?? getLastCertificateId())
            if try db.run(history.update(isValid <- value)) > 0 {
                print("changed")
            }
        }catch{
            print("Error: \(error)")
        }
    }
}
