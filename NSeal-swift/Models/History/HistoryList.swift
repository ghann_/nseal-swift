//
//  HistoryList.swift
//  NSeal-swift
//
//  Created by ma.ghani on 3/13/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import Foundation

struct HistoryList {
    var date: String
    var histories: [History]
    
    init(date: String, histories: [History]) {
        self.date = date
        self.histories = histories
    }
    
    static func getHistoryList() -> [HistoryList] {
        let allHistory = History.getAllHistory()!
        let arrayOfDate = allHistory.map{ $0.date.withoutTime }
        
        var arrUniqueDate: [String] = []
        for date in arrayOfDate{
            if !arrUniqueDate.contains(date){
                arrUniqueDate.append(date)
            }
        }
        
        var result: [HistoryList] = []
        for uniqeDate in arrUniqueDate {
            var arrHistoryMatched: [History] = []
            for i in 0..<allHistory.count{
                if uniqeDate == allHistory[i].date.withoutTime {
                    arrHistoryMatched.append(allHistory[i])
                }
            }
            let matchedHistories = HistoryList(date: uniqeDate, histories: arrHistoryMatched)
            result.append(matchedHistories)
        }
        return result
    }
}
