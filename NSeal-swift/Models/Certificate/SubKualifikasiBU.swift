//
//  SubKualifikasiBU.swift
//  NSeal-swift
//
//  Created by ma.ghani on 4/2/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import Foundation

class SubKualifikasiBU{
    var nomorUrut: Int
    var kualifikasi: String
    var kodeSubklasifikasi: String
    var deskripsiSubkualifikasi: String
    var tahunKD: String
    var nilaiKD: String
    
    init(nomorUrut: Int, kualifikasi: String, kodeSubklasifikasi: String, deskripsiSubkualifikasi: String, tahunKD: String, nilaiKD: String) {
        self.nomorUrut = nomorUrut
        self.kualifikasi = kualifikasi
        self.kodeSubklasifikasi = kodeSubklasifikasi
        self.deskripsiSubkualifikasi = deskripsiSubkualifikasi
        self.tahunKD = tahunKD
        self.nilaiKD = nilaiKD
    }
}
