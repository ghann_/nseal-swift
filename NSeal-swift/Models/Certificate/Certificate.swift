//
//  Certificate.swift
//  NSeal-swift
//
//  Created by ma.ghani on 3/29/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import Foundation

class Certificate{
    
    typealias CertificateHandler = (Bool, Certificate?, String?) -> ()
    
    var type: CertificateType
    var nama: String
    var subKualifikasi: String
    
    var alamat: String
    var subBidang: String
    var kualifikasi: String
    var kompetensiKerja: [KompetensiKerja]

    var namaBadanUsaha: String
    var namaPenanggungJawab: String
    var kabupaten: String
    var provinsi: String
    var nomorTelepon: String
    var email: String
    var npwp: String
    var jenisBadanUsaha: String
    var kategoriBadanUsaha: String
    var kekayaanBersih: String
    var kodePos: String
    var nomorFax: String
    var klasifikasiSBU: String
    var golonganSBU: String
    var subKualifikasiBU: [SubKualifikasiBU]
    
    var tempatDitetapkan: String
    var tanggalDitetapkan: String
    var jabatanPenandatangan: String
    var namaPenandatangan: String
    var asosiasi: String
    var nomorBeritaAsosiasi: String
    var nomorRegistrasi: String
    
    var idUnikPersonal: String
    var tanggalCetak: String
    var tanggalHabis: String
    
    fileprivate init(type: CertificateType,
         nama: String = "",
         subKualifikasi: String = "",
         
         alamat: String = "",
         subBidang: String = "",
         kualifikasi: String = "",
         kompetensiKerja: [KompetensiKerja] = [],

         namaBadanUsaha: String = "",
         namaPenanggungJawab: String = "",
         kabupaten: String = "",
         provinsi: String = "",
         nomorTelepon: String = "",
         email: String = "",
         npwp: String = "",
         jenisBadanUsaha: String = "",
         kategoriBadanUsaha: String = "",
         kekayaanBersih: String = "",
         kodePos: String = "",
         nomorFax: String = "",
         klasifikasiSBU: String = "",
         golonganSBU: String = "",
         subKualifikasiBU: [SubKualifikasiBU] = [],
         
         tempatDitetapkan: String = "",
         tanggalDitetapkan: String = "",
         jabatanPenandatangan: String = "",
         namaPenandatangan: String = "",
         asosiasi: String = "",
         nomorBeritaAsosiasi: String = "",
         nomorRegistrasi: String = "",
        
         idUnikPersonal: String = "",
         tanggalCetak: String = "",
         tanggalHabis: String = ""
        ){
        self.type = type
        self.nama = nama
        self.subKualifikasi = subKualifikasi
        
        self.alamat = alamat
        self.subBidang = subBidang
        self.kualifikasi = kualifikasi
        self.kompetensiKerja = kompetensiKerja
        
        self.namaBadanUsaha = namaBadanUsaha
        self.namaPenanggungJawab = namaPenanggungJawab
        self.kabupaten = kabupaten
        self.provinsi = provinsi
        self.nomorTelepon = nomorTelepon
        self.email = email
        self.npwp = npwp
        self.jenisBadanUsaha = jenisBadanUsaha
        self.kategoriBadanUsaha = kategoriBadanUsaha
        self.kekayaanBersih = kekayaanBersih
        self.kodePos = kodePos
        self.nomorFax = nomorFax
        self.klasifikasiSBU = klasifikasiSBU
        self.golonganSBU = golonganSBU
        self.subKualifikasiBU = subKualifikasiBU
        
        self.tempatDitetapkan = tempatDitetapkan
        self.tanggalDitetapkan = tanggalDitetapkan
        self.jabatanPenandatangan = jabatanPenandatangan
        self.namaPenandatangan = namaPenandatangan
        self.asosiasi = asosiasi
        self.nomorBeritaAsosiasi = nomorBeritaAsosiasi
        self.nomorRegistrasi = nomorRegistrasi
        
        self.idUnikPersonal = idUnikPersonal
        self.tanggalCetak = tanggalCetak
        self.tanggalHabis = tanggalHabis
    }

    convenience init(SKACertificateName nama: String,
                     subKualifikasi: String,
                     nomorRegistrasi: String,
                     tempatDitetapkan: String,
                     tanggalDitetapkan: String,
                     jabatanPenandatangan: String,
                     namaPenandatangan: String,
                     asosiasi: String,
                     nomorBeritaAsosiasi: String,
                     idUnikPersonal: String,
                     tanggalCetak: String,
                     tanggalHabis: String){

        self.init(type: .SKA1,
                  nama: nama,
                  subKualifikasi: subKualifikasi,
                  tempatDitetapkan: tempatDitetapkan,
                  tanggalDitetapkan: tanggalDitetapkan,
                  jabatanPenandatangan: jabatanPenandatangan,
                  namaPenandatangan: namaPenandatangan,
                  asosiasi: asosiasi,
                  nomorBeritaAsosiasi: nomorBeritaAsosiasi,
                  nomorRegistrasi: nomorRegistrasi,
                  idUnikPersonal: idUnikPersonal,
                  tanggalCetak: tanggalCetak,
                  tanggalHabis: tanggalHabis)
    }
    
    convenience init(SKTCertificateName nama: String,
                     alamat: String,
                     subBidang: String,
                     kualifikasi: String,
                     tempatDitetapkan: String,
                     tanggalDitetapkan: String,
                     jabatanPenandatangan: String,
                     namaPenandatangan: String,
                     nomorRegistrasi: String,
                     asosiasi: String,
                     kompetensiKerja: [KompetensiKerja],
                     idUnikPersonal: String,
                     tanggalCetak: String,
                     tanggalHabis: String){
        
        self.init(type: .SKT1,
                  nama: nama,
                  alamat: alamat,
                  subBidang: subBidang,
                  kualifikasi: kualifikasi,
                  kompetensiKerja: kompetensiKerja,
                  tempatDitetapkan: tempatDitetapkan,
                  tanggalDitetapkan: tanggalDitetapkan,
                  jabatanPenandatangan: jabatanPenandatangan,
                  namaPenandatangan: namaPenandatangan,
                  asosiasi: asosiasi,
                  nomorRegistrasi: nomorRegistrasi,
                  idUnikPersonal: idUnikPersonal,
                  tanggalCetak: tanggalCetak,
                  tanggalHabis: tanggalHabis)
    }
    
    convenience init(SBUCertificateName namaBadanUsaha: String,
                     namaPenanggungJawab: String,
                     alamat: String,
                     kabupaten: String,
                     provinsi: String,
                     nomorTelepon: String,
                     email: String,
                     npwp: String,
                     jenisBadanUsaha: String,
                     kategoriBadanUsaha: String,
                     kekayaanBersih: String,
                     kodePos: String,
                     nomorFax: String,
                     nomorRegistrasi: String,
                     tempatDitetapkan: String,
                     tanggalDitetapkan: String,
                     jabatanPenandatangan: String,
                     namaPenandatangan: String,
                     klasifikasiSBU: String,
                     golonganSBU: String,
                     asosiasi: String,
                     subKualifikasiSBU: [SubKualifikasiBU],
                     idUnikPersonal: String,
                     tanggalCetak: String,
                     tanggalHabis: String){
        
        self.init(type: .SBU1,
                  alamat: alamat,
                  namaBadanUsaha: namaBadanUsaha,
                  namaPenanggungJawab: namaPenanggungJawab,
                  kabupaten: kabupaten,
                  provinsi: provinsi,
                  nomorTelepon: nomorTelepon,
                  email: email,
                  npwp: npwp,
                  jenisBadanUsaha: jenisBadanUsaha,
                  kategoriBadanUsaha: kategoriBadanUsaha,
                  kekayaanBersih: kekayaanBersih,
                  kodePos: kodePos,
                  nomorFax: nomorFax,
                  klasifikasiSBU: klasifikasiSBU,
                  golonganSBU: golonganSBU,
                  subKualifikasiBU: subKualifikasiSBU,
                  tempatDitetapkan: tempatDitetapkan,
                  tanggalDitetapkan: tanggalDitetapkan,
                  jabatanPenandatangan: jabatanPenandatangan,
                  namaPenandatangan: namaPenandatangan,
                  asosiasi: asosiasi,
                  nomorRegistrasi: nomorRegistrasi,
                  idUnikPersonal: idUnikPersonal,
                  tanggalCetak: tanggalCetak,
                  tanggalHabis: tanggalHabis)
    }
    
    static func getSKACertificate(withDisplayData displayDataList: [NSealDisplayData], handler: CertificateHandler) {
        let arrayData = displayDataList.map{ $0.text! }
        
        print("\n\n----\nSKA arrayData:\n")
        print(arrayData)
        print("----\n\n")
        
        guard arrayData.count > 10 else {
            handler(false, nil, "Invalid QR Code".localized)
            return
        }
        
        let ska = Certificate(SKACertificateName: arrayData[0],
                              subKualifikasi: arrayData[1],
                              nomorRegistrasi: arrayData[2],
                              tempatDitetapkan: arrayData[3],
                              tanggalDitetapkan: arrayData[4],
                              jabatanPenandatangan: arrayData[5],
                              namaPenandatangan: arrayData[6],
                              asosiasi: arrayData[7],
                              nomorBeritaAsosiasi: arrayData[8],
                              idUnikPersonal: arrayData[9],
                              tanggalCetak: arrayData[10],
                              tanggalHabis: arrayData[11])
        handler(true, ska, nil)
    }
    
    static func getSKTCertificate(withDisplayData displayDataList: [NSealDisplayData], handler: CertificateHandler){
        let arrayData = displayDataList.map{ $0.type == 0 ? $0.text! : "" }
        
        print("\n\n----\nSKT arrayData:\n")
        print(arrayData)
        print("----\n\n")
        
        guard arrayData.count > 12 else {
            handler(false, nil, "Invalid QR Code".localized)
            return
        }
        
        var arrayKompetensiKerja: [KompetensiKerja] = []
        let kompetensiKerjaList = displayDataList[11].table as! [[String]]
        for kompetensiKerja in kompetensiKerjaList {
            let temp = KompetensiKerja(nomorUrut: Int(kompetensiKerja[0])!, deskripsi:  kompetensiKerja[1])
            arrayKompetensiKerja.append(temp)
        }

        let skt = Certificate(SKTCertificateName: arrayData[0],
                              alamat: arrayData[1],
                              subBidang: arrayData[2],
                              kualifikasi: arrayData[3],
                              tempatDitetapkan: arrayData[5],
                              tanggalDitetapkan: arrayData[6],
                              jabatanPenandatangan: arrayData[7],
                              namaPenandatangan: arrayData[8],
                              nomorRegistrasi: arrayData[9],
                              asosiasi: arrayData[10],
                              kompetensiKerja: arrayKompetensiKerja,
                              idUnikPersonal: arrayData[12],
                              tanggalCetak: arrayData[13],
                              tanggalHabis: arrayData[4])
         handler(true, skt, nil)
    }
    
    static func getSBUCertificate(withDisplayData displayDataList: [NSealDisplayData], handler: CertificateHandler){
        let arrayData = displayDataList.map{ $0.type == 0 ? $0.text! : "" }
        
        print("\n\n----\nSBU arrayData:\n")
        print(arrayData)
        print("----\n\n")
        
        guard arrayData.count > 23 else {
            handler(false, nil, "Invalid QR Code".localized)
            return
        }
        
        var arraySubklasifikasiBU: [SubKualifikasiBU] = []
        let subklasifikasiBUList = displayDataList[21].table as! [[String]]
        for subKlasifikasi in subklasifikasiBUList {
            let temp = SubKualifikasiBU(nomorUrut: Int(subKlasifikasi[0])!, kualifikasi: subKlasifikasi[1],
                                        kodeSubklasifikasi: subKlasifikasi[2], deskripsiSubkualifikasi: subKlasifikasi[3],
                                        tahunKD: subKlasifikasi[4], nilaiKD: subKlasifikasi[5])
            arraySubklasifikasiBU.append(temp)
        }
        
        let sbu = Certificate(SBUCertificateName: arrayData[0],
                              namaPenanggungJawab: arrayData[1],
                              alamat: arrayData[2],
                              kabupaten: arrayData[3],
                              provinsi: arrayData[4],
                              nomorTelepon: arrayData[5],
                              email: arrayData[6],
                              npwp: arrayData[7],
                              jenisBadanUsaha: arrayData[8],
                              kategoriBadanUsaha: arrayData[9],
                              kekayaanBersih: arrayData[10],
                              kodePos: arrayData[11],
                              nomorFax: arrayData[12],
                              nomorRegistrasi: arrayData[13],
                              tempatDitetapkan: arrayData[14],
                              tanggalDitetapkan: arrayData[15],
                              jabatanPenandatangan: arrayData[16],
                              namaPenandatangan: arrayData[17],
                              klasifikasiSBU: arrayData[18],
                              golonganSBU: arrayData[19],
                              asosiasi: arrayData[20],
                              subKualifikasiSBU: arraySubklasifikasiBU,
                              idUnikPersonal: arrayData[22],
                              tanggalCetak: arrayData[23],
                              tanggalHabis: arrayData[24])
        handler(true, sbu, nil)
    }
}
