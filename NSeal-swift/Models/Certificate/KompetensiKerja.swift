//
//  KompetensiKerja.swift
//  NSeal-swift
//
//  Created by ma.ghani on 4/2/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import Foundation

class KompetensiKerja{
    var nomorUrut: Int
    var deskripsi: String
    
    init(nomorUrut: Int, deskripsi: String) {
        self.nomorUrut = nomorUrut
        self.deskripsi = deskripsi
    }
}
