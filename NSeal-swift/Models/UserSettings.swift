//
//  UserSettings.swift
//  NSeal-swift
//
//  Created by ma.ghani on 3/8/18.
//  Copyright © 2018 Mirai Apps. All rights reserved.
//

import Foundation
import SQLite

struct UserSettings{
    var soundEffect: Bool
    var vibration: Bool
    var localization: Localization
    var certificateDisplayResult: CertificateDisplayResult
    
    init(soundEffectOption: Bool, vibrationOption: Bool, localization: Localization, certificateDisplayResult: CertificateDisplayResult) {
        self.soundEffect = soundEffectOption
        self.vibration = vibrationOption
        self.localization = localization
        self.certificateDisplayResult = certificateDisplayResult
    }
    
    static func createDatabaseTable() {
        do{
            let db = try Connection("\(Constants.databasePath)/db.sqlite3")
            let userSettingsTable = Table("userSettings")
            let soundEffect = Expression<Bool>("soundEffect")
            let vibration = Expression<Bool>("vibration")
            let localization = Expression<String>("localization")
            let certificateDisplayResult = Expression<String>("certificateDisplayResult")
            
            try db.run(userSettingsTable.create(ifNotExists: false) { t in
                t.column(soundEffect)
                t.column(vibration)
                t.column(localization)
                t.column(certificateDisplayResult)
            })
            
            let insert = userSettingsTable.insert(soundEffect <- true, vibration <- true, localization <- "Indonesia", certificateDisplayResult <- "default")
            _ = try db.run(insert)
        }catch{
            print("Error: \(error)")
            return
        }
    }
    
    static func getUserSettings() -> UserSettings? {
        do{
            let db = try Connection("\(Constants.databasePath)/db.sqlite3")
            let userSettingsTable = Table("userSettings")
            let soundEffect = Expression<Bool>("soundEffect")
            let vibration = Expression<Bool>("vibration")
            let localization = Expression<String>("localization")
            let certificateDisplayResult = Expression<String>("certificateDisplayResult")
            
            var userSettings: UserSettings?
            for settings in try db.prepare(userSettingsTable) {
                userSettings = UserSettings(soundEffectOption: settings[soundEffect],
                                            vibrationOption: settings[vibration],
                                            localization: Localization(rawValue: settings[localization])!,
                                            certificateDisplayResult: CertificateDisplayResult(rawValue: settings[certificateDisplayResult])!)
            }
            return userSettings
        }catch{
            print("Get UserSetting error: \(error)")
            return nil
        }
    }
    
    static func saveSoundEffect(enable: Bool) {
        do {
            let db = try Connection("\(Constants.databasePath)/db.sqlite3")
            let userSettingsTable = Table("userSettings")
            let soundEffect = Expression<Bool>("soundEffect")
            if try db.run(userSettingsTable.update(soundEffect <- enable )) > 0 {
                print("soundEffect updated to \(enable)")
            }
        } catch {
            print("Update failed: \(error)")
            return
        }
    }
    
    static func saveVibration(enable: Bool) {
        do {
            let db = try Connection("\(Constants.databasePath)/db.sqlite3")
            let userSettingsTable = Table("userSettings")
            let vibration = Expression<Bool>("vibration")
            
            if try db.run(userSettingsTable.update(vibration <- enable )) > 0 {
                print("Vibration updated to \(enable)")
            }
        } catch {
            print("Update failed: \(error)")
            return
        }
    }
    
    static func saveLocalization(localizationString: String) {
        do {
            let db = try Connection("\(Constants.databasePath)/db.sqlite3")
            let userSettingsTable = Table("userSettings")
            let localization = Expression<String>("localization")
            
            if try db.run(userSettingsTable.update(localization <- localizationString )) > 0 {
                print("Localization updated to \(localizationString)")
            } else {
                print("Localization not updated")
            }
        } catch {
            print("Update failed: \(error)")
            return
        }
    }
    
    static func saveDisplayResult(certificateDisplayResultString: String) {
        do {
            let db = try Connection("\(Constants.databasePath)/db.sqlite3")
            let userSettingsTable = Table("userSettings")
            let certificateDisplayResult = Expression<String>("certificateDisplayResult")
            
            if try db.run(userSettingsTable.update(certificateDisplayResult <- certificateDisplayResultString )) > 0 {
                print("certificateDisplayResult updated to \(certificateDisplayResultString)")
            } else {
                print("certificateDisplayResult not updated")
            }
        } catch {
            print("Update failed: \(error)")
            return
        }
    }
}

