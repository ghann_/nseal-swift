//
//  nSealVerifier.h
//  NSealSDK
//
//  Created by Lackern Xu on 30/12/17.
//  Copyright © 2017 Lackern Xu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSealResult.h"

/**
 Description
 */
@interface NSealVerifier : NSObject

/**
 nSeal SDK Core

 @param codeString QR code string
 @return NSealResult
 */

//-(NSealVerifier*)initWithDeltaURLString:(NSString*)urlString;
//-(BOOL)downloadDelta;
-(NSealResult *)verifyNSealQRCodeString:(NSString *)codeString;

-(NSString *)defaultFailedNoPayloadHTMLString;

@end
