//
//  NSealException.h
//  NSealSDK
//
//  Created by Lackern on 8/2/18.
//  Copyright © 2018 Netrust Pte Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

//typedef NS_ENUM(NSInteger, NSealCode){
//    NSealCodeDataBaseInvalid, // 0
//    NSealCodePayloadInvalid, // 1
//    NSealCodePayloadSignatureMissing,
//    NSealCodePayloadDataMissing
//};

#define NSealErrorCodeDataBaseInvalid 0
#define NSealErrorCodePayloadInvalid 1
#define NSealErrorCodePayloadSignatureMissing 2
#define NSealErrorCodePayloadDataMissing 3

#define NSealErrorDescriptionDataBaseInvalid @"Databse signature miss-matched, Database is missing or has been tampered with."
#define NSealErrorDescriptionPayloadInvalid @"nSeal payload not found, invalid nSeal scanned."
#define NSealErrorDescriptionPayloadSignatureMissing @"nSeal signature not found, corrupted nSeal scanned."
#define NSealErrorDescriptionPayloadDataMissing @"nSeal data not found, corrupted nSeal scanned."


@interface NSealException : NSException

@property (readonly) NSInteger errorCode;
@property (nonatomic, retain, readonly) NSString *errorDescription;

+ (NSealException*)exceptionWithErrorDescription:(NSString*)description errorCode:(NSInteger)errorCode;

// shortcut methods

@end
