//
//  NSealResult.h
//  NSealSDK
//
//  Created by Lackern on 7/2/18.
//  Copyright © 2018 Netrust Pte Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSealRevocationResult.h"

@class NSeal;

@interface NSealResult : NSObject

// Passed
@property (nonatomic) BOOL isValid;
// templateInfo
@property (nonatomic) BOOL isTemplateMissing; //

// Failed Info
@property (nonatomic) BOOL isTampered; // signature verification failed
@property (nonatomic) BOOL isTooEarly; // validity have not start
@property (nonatomic) BOOL isExpired; // validity ended
@property (nonatomic) BOOL isRevoked; // revoked

@property (nonatomic, retain) NSDate * issuedDate; // UTC
@property (nonatomic, retain) NSDate * validFromDate; // UTC
@property (nonatomic, retain) NSDate * validToDate; // UTC
@property (nonatomic, retain) NSealRevocationResult * revocationResult; // UTC

@property (nonatomic, retain) NSString * sealID;
@property (nonatomic, retain) NSString * templateID;
@property (nonatomic, retain) NSString * signerInfo;
@property (nonatomic, retain) NSArray  * displayDataList;

// @property (nonatomic, retain) NSString * HTMLResultOnly;
@property (nonatomic, retain) NSString * HTMLResultWithCertificate;
@property (nonatomic, retain) NSString * HTMLCertificateOnly;

// Extra info
-(NSString*)getValidFromDateInfoString;
-(NSString*)getValidToDateInfoString;
-(NSString*)getRevocationInfoString;

@end
