//
//  NSealRevocationResult.h
//  NSealSDK
//
//  Created by Lackern on 5/2/18.
//  Copyright © 2018 Netrust Pte Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSealRevocationResult : NSObject

@property (nonatomic, retain) NSString * sealID;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * reasonCode;
@property (nonatomic, retain) NSString * reason;

@end
