//
//  NSealSDK.h
//  NSealSDK
//
//  Created by Lackern on 31/1/18.
//  Copyright © 2018 Netrust Pte Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for NSealSDK.
FOUNDATION_EXPORT double NSealSDKVersionNumber;

//! Project version string for NSealSDK.
FOUNDATION_EXPORT const unsigned char NSealSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <NSealSDK/PublicHeader.h>

//Public: The interface is finalized and meant to be used by your product’s clients. A public header is included in the product as readable source code without restriction.
//
//Private: The interface isn’t intended for your clients or it’s in early stages of development. A private header is included in the product, but it’s marked “private”. Thus the symbols are visible to all clients, but clients should understand that they're not supposed to use them.
//
//Project: The interface is for use only by implementation files in the current project. A project header is not included in the target, except in object code. The symbols are not visible to clients at all, only to you.

#import <NSealSDK/NSealVerifier.h>
#import <NSealSDK/NSealResult.h>
#import <NSealSDK/NSealRevocationResult.h>
#import <NSealSDK/NSealException.h>
#import <NSealSDK/NSealDisplayData.h>
