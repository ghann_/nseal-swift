//
//  NSealDisplayData.h
//  NSealSDK
//
//  Created by Lackern on 22/3/18.
//  Copyright © 2018 Netrust Pte Ltd. All rights reserved.
//

#define NSealDisplayDataTypeText 0
#define NSealDisplayDataTypeImage 1
#define NSealDisplayDataTypeTable 2

#import <Foundation/Foundation.h>

@interface NSealDisplayData : NSObject

@property (nonatomic) NSInteger type;

@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSString * imageBase64;
@property (nonatomic, retain) NSArray * table; // 2D array for rows and columns

@end
